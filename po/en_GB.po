# English translations for GNU libbtctl package.
# Copyright (C) 2003 THE GNU libbtctl'S COPYRIGHT HOLDER
# This file is distributed under the same license as the GNU libbtctl package.
# Edd Dumbill <edd@usefulinc.com>, 2003.
#
msgid ""
msgstr ""
"Project-Id-Version: GNU libbtctl 0.3.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2004-06-18 19:57+0100\n"
"PO-Revision-Date: 2003-02-21 22:10+0000\n"
"Last-Translator: Edd Dumbill <edd@usefulinc.com>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=ISO-8859-15\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: src/btctlimpl.c:585 src/btctlimpl.c:682 src/btctlimpl.c:737
msgid "Can't open RFCOMM control socket"
msgstr "Can't open RFCOMM control socket"

#: src/btctlimpl.c:591 src/btctlimpl.c:688 src/btctlimpl.c:743
msgid "Can't allocate memory"
msgstr "Can't allocate memory"

#: src/btctlimpl.c:598 src/btctlimpl.c:696
msgid "Can't get device list"
msgstr "Can't get device list"

#: src/btctlimpl.c:819
msgid "No bluetooth adapter found"
msgstr ""

#: src/btctlimpl.c:828
msgid "Couldn't access the bluetooth adapter"
msgstr ""

#: src/btctlimpl.c:836
msgid "Couldn't create a connection to the device"
msgstr ""

#: src/btctlimpl.c:851
msgid "Couldn't connect to the device"
msgstr ""

#: src/btctlimpl.c:869
msgid "Couldn't get the status of the device"
msgstr ""
