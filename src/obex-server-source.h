#ifndef _BTCTL_OBEXSERVER_SOURCE_H
#define _BTCTL_OBEXSERVER_SOURCE_H

#include <sys/socket.h>
#include <bluetooth/bluetooth.h>
#include <bluetooth/hci.h>

#include <glib.h>
#include <openobex/obex.h>

G_BEGIN_DECLS

typedef struct _BtctlObexserverEvent BtctlObexserverEvent;

struct _BtctlObexserverEvent {
    gint event;
    gint cmd;
    gpointer data;
};

typedef struct _BtctlObexserverSource BtctlObexserverSource;

typedef gboolean (*BtctlObexserverSourceFunc) 
    (struct _BtctlObexserverSource *source, BtctlObexserverEvent *event,
     gpointer data);

struct _BtctlObexserverSource {
    GSource gsource;
    GPollFD fd;
    GPollFD serverfd;
    gboolean initialised;
    obex_t *handle;
    bdaddr_t peer_bdaddr;
    gchar peer_name[20];
    BtctlObexserverSourceFunc callback;
    gpointer callback_data;
};


enum {
    BTCTL_OBEXSERVER_PREPUSH,
    BTCTL_OBEXSERVER_PUSH
};


#define btctl_obexserver_source_ref(x) g_source_ref((GSource*)x)
#define btctl_obexserver_source_unref(x) g_source_unref((GSource*)x)

void btctl_obexserver_source_destroy (BtctlObexserverSource *source);
void btctl_obexserver_source_attach (BtctlObexserverSource *source,
        GMainContext *ctxt);
BtctlObexserverSource * btctl_obexserver_source_new (void);
void btctl_obexserver_source_set_callback (BtctlObexserverSource *source,
        BtctlObexserverSourceFunc func, gpointer data,
        GDestroyNotify notify);
void btctl_obexserver_source_client_close (BtctlObexserverSource *s);
void btctl_obexserver_source_client_cancel (BtctlObexserverSource *s);


G_END_DECLS

#endif /* _BTCTL_OBEXSERVER_SOURCE_H */
