/*
 * libbtctl -- GObject Bluetooth libraries
 * Copyright (C) 2003-2004 Edd Dumbill <edd@usefulinc.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

#include <config.h>

#include <stdio.h>
#include <glib.h>
#include <glib-object.h>
#include <assert.h>

#include "btctl.h"
#include "btctlimpl.h"
#include "btctl-marshal.h"

static gpointer         parent_class = NULL;

/* gobject prototypes */

static void btctl_controller_class_init (BtctlControllerClass *class);
static void btctl_controller_init (BtctlController *bc);
static void btctl_controller_finalize(GObject *obj);

/* gtk signal defns */

enum {
	ADD_DEVICE_SIGNAL,
	DEVICE_NAME_SIGNAL,
	STATUS_CHANGE_SIGNAL,
	ADD_DEVICE_SERVICE_SIGNAL,
	LAST_SIGNAL
};

enum
{
  PROP_0,  /* gobject convention */

  PROP_BTCTL_HCI_DEVICE
};

static gint btctl_controller_signals[LAST_SIGNAL] = { 0 } ;

G_DEFINE_TYPE(BtctlController, btctl_controller, G_TYPE_OBJECT)

static void btctl_set_property(GObject      *object,
			       guint         prop_id,
			       const GValue *value,
			       GParamSpec   *pspec)
{
	BtctlController *bc = BTCTL_CONTROLLER(object);

	if (bc->init_err)
		/* An error has previously occurd during init, bail out. */
		return;
	
	switch(prop_id)
	{
		case PROP_BTCTL_HCI_DEVICE:
			btctl_controller_impl_set_hci_device
				(bc, g_value_get_string(value), &bc->init_err);
			break;
	}
}

static void
btctl_controller_class_init (BtctlControllerClass *klass)
{
	GObjectClass *object_class;

	parent_class = g_type_class_ref(G_TYPE_OBJECT);

	object_class=(GObjectClass*)klass;

	btctl_controller_signals[ADD_DEVICE_SIGNAL] = 
        g_signal_new ("add_device",
            G_OBJECT_CLASS_TYPE(klass),
			G_SIGNAL_RUN_FIRST,
			G_STRUCT_OFFSET(BtctlControllerClass, add_device),
            NULL /* accu */, NULL,
			btctl_marshal_VOID__STRING_UINT,
			G_TYPE_NONE, 2, G_TYPE_STRING, G_TYPE_UINT);

    btctl_controller_signals[DEVICE_NAME_SIGNAL] =
        g_signal_new ("device_name",
            G_OBJECT_CLASS_TYPE(klass),
			G_SIGNAL_RUN_FIRST,
			G_STRUCT_OFFSET(BtctlControllerClass, device_name),
            NULL /* accu */, NULL,
			btctl_marshal_VOID__STRING_STRING,
			G_TYPE_NONE, 2, G_TYPE_STRING, G_TYPE_STRING);


	btctl_controller_signals[STATUS_CHANGE_SIGNAL] =
        g_signal_new ("status_change",
            G_OBJECT_CLASS_TYPE(klass),
			G_SIGNAL_RUN_FIRST,
			G_STRUCT_OFFSET(BtctlControllerClass, status_change),
            NULL /* accu */, NULL,
			g_cclosure_marshal_VOID__INT,
			G_TYPE_NONE, 1, G_TYPE_INT);

	btctl_controller_signals[ADD_DEVICE_SERVICE_SIGNAL] =
          g_signal_new ("add_device_service",
			G_OBJECT_CLASS_TYPE(klass),
			G_SIGNAL_RUN_FIRST,
			G_STRUCT_OFFSET(BtctlControllerClass, add_device_service),
			NULL, NULL,
			btctl_marshal_VOID__STRING_STRING_UINT_UINT,
			G_TYPE_NONE, 4, G_TYPE_STRING, G_TYPE_STRING,
			G_TYPE_UINT, G_TYPE_UINT);
	
	object_class->set_property = btctl_set_property;
	object_class->finalize = btctl_controller_finalize;
	
	g_object_class_install_property
		(object_class,
		 PROP_BTCTL_HCI_DEVICE,
		 g_param_spec_string ("hci_device",
				      "HCI device",
				      "HCI device",
				      NULL,
				      G_PARAM_WRITABLE |
				      G_PARAM_CONSTRUCT_ONLY));
	
	klass->add_device=NULL;
}

static void
btctl_controller_init (BtctlController *bc)
{
	bc->init_err = NULL;
	bc->source = btctl_discovery_source_new ();
	btctl_controller_impl_init_source (bc);
	btctl_discovery_source_attach (bc->source, NULL);
}

/**
 * btctl_error_quark:
 * 
 * Get the error quark for btctl functions.
 *
 * If the quark does not yet exist, create it.
 * 
 * Return value: Quark associated with btctl errors.
 **/
GQuark
btctl_error_quark(void)
{
  static GQuark quark = 0;
  if (quark == 0)
    quark = g_quark_from_static_string ("btctl-error-quark");
  return quark;
}

/**
 * btctl_controller_new:
 * @hci_device: Bluetooth HCI device string (e.g. "hci0") or NULL.
 *
 * Create a new Bluetooth controller object.  This will attempt to open
 * an HCI socket to the default Bluetooth device.  Use
 * btctl_controller_is_initialised() to check whether this was successful.
 *
 * Return value: a pointer to the controller object.
 **/
BtctlController*
btctl_controller_new(const char *hci_device)
{
	return BTCTL_CONTROLLER (g_object_new (btctl_controller_get_type(),
					       "hci_device", hci_device,
					       NULL));
}

static void
btctl_controller_finalize(GObject *obj)
{
	BtctlController *bc;

	bc=BTCTL_CONTROLLER(obj);

	/* custom finalize stuff goes here
	 */
	btctl_discovery_source_destroy (bc->source);
	btctl_discovery_source_unref (bc->source);
	/* call superclass' destructor */
	G_OBJECT_CLASS(parent_class)->finalize(obj);

	if(bc->init_err)
		g_error_free(bc->init_err);
}

static void
btctl_controller_emit_add_device(BtctlController *bc, gchar  *bdaddr,
        guint clsid)
{
	g_signal_emit (G_OBJECT (bc),
		       btctl_controller_signals[ADD_DEVICE_SIGNAL],
		       0, bdaddr, clsid);
}

static void
btctl_controller_emit_device_name(BtctlController *bc,
        gchar  *bdaddr, gchar *name)
{
	g_signal_emit (G_OBJECT (bc),
		       btctl_controller_signals[DEVICE_NAME_SIGNAL],
		       0, bdaddr, name);
}

static void
btctl_controller_emit_status_change(BtctlController *bc, gint status)
{
	g_signal_emit (G_OBJECT (bc),
		       btctl_controller_signals[STATUS_CHANGE_SIGNAL],
		       0, status);
}

static void
btctl_controller_emit_device_service(BtctlController *bc, gchar *bdaddr,
				      gchar *name, guint classid, guint port)
{
	g_signal_emit (G_OBJECT(bc),
		       btctl_controller_signals[ADD_DEVICE_SERVICE_SIGNAL],
		       0, bdaddr, name, classid, port);
}

/* exported but private */

void
btctl_controller_report_status(BtctlController *bc, gint status)
{
	btctl_controller_emit_status_change(bc, status);
}

void
btctl_controller_got_device(BtctlController *bc, gchar *bdaddr,
        guint clsid)
{
	btctl_controller_emit_add_device(bc, bdaddr, clsid);
}

void
btctl_controller_got_device_name(BtctlController *bc, gchar *bdaddr,
        gchar *name)
{
	btctl_controller_emit_device_name(bc, bdaddr, name);
}

void
btctl_controller_got_device_service(BtctlController *bc, gchar *bdaddr,
			      gchar *name, guint classid, guint port)
{
	btctl_controller_emit_device_service(bc, bdaddr, name, classid, port);
}

/* substantial public methods */

/**
 * btctl_controller_get_discoverable:
 * @bc: Bluetooth controller object.
 *
 * Returns discoverable status for the local device.
 *
 * Return value: TRUE if the local device is discoverable.
 **/
gboolean
btctl_controller_get_discoverable(BtctlController *bc, GError **err)
{
	return btctl_controller_impl_get_discoverable(bc, err);
}

/**
 * btctl_controller_set_discoverable:
 * @bc: Bluetooth controller object.
 * @discoverable: TRUE if device is discoverable
 *
 * Enables/disables local device to be discoverable.
 **/
void
btctl_controller_set_discoverable(BtctlController *bc, gboolean discoverable,
				  GError **err)
{
	btctl_controller_impl_set_discoverable(bc, discoverable, err);
}

/**
 * btctl_controller_discover_devices:
 * @bc: Bluetooth controller object.
 * @err: An error message (if applicable).
 *
 * Commence a synchronous device discovery cycle.
 *
 **/
void
btctl_controller_discover_devices(BtctlController *bc, GError **err)
{
	bc->cancel = 0;
	btctl_controller_impl_cmd_scan(bc, err);
}

/**
 * btctl_controller_cancel_discovery:
 * @bc: Bluetooth controller object.
 *
 * Cancel an asynchronous discovery cycle.  Will only work if inquiry
 * cancellation support is present in the kernel.
 **/
void
btctl_controller_cancel_discovery(BtctlController *bc)
{
	bc->cancel = 1;
}

/**
 * btctl_controller_list_rfcomm_connections:
 * @bc: Bluetooth controller object.
 *
 * Dump established rfcomm connections to the terminal.
 **/
void
btctl_controller_list_rfcomm_connections(BtctlController *bc)
{
	btctl_controller_impl_list_rfcomm_connections(bc);
}

/**
 * btctl_controller_get_established_rfcomm_connection:
 * @bc: Bluetooth controller object.
 * @bdstr: Bluetooth address of destination device.
 * @channel: RFCOMM channel.
 *
 * Find rfcomm device number (ie. N for /dev/rfcommN) connected to the
 * destination device on the specified channel. Returns
 * #BTCTL_RFCOMM_NO_DEVICE is no device is available, or
 * #BTCTL_RFCOMM_DEVICE_IN_USE if a device is available but already in
 * use.
 *
 * Return value: rfcomm device number.
 **/
int
btctl_controller_get_established_rfcomm_connection(BtctlController *bc,
		const gchar *bdstr, guint channel)
{
	return btctl_controller_impl_get_established_rfcomm_connection(bc, bdstr, channel);
}

/**
 * btctl_controller_establish_rfcomm_connection:
 * @bc: Bluetooth controller object.
 * @bdstr: Bluetooth address of destination device.
 * @channel: RFCOMM channel.
 *
 * Link an rfcomm device to the destination device. Returns
 * #BTCTL_RFCOMM_NO_DEVICE if the connection cannot be made.
 *
 * Return value: rfcomm device number.
 **/
int
btctl_controller_establish_rfcomm_connection(BtctlController *bc, const gchar *bdstr, guint channel)
{
	return btctl_controller_impl_establish_rfcomm_connection(bc, bdstr, channel);
}

/**
 * btctl_controller_scan_for_service:
 * @bc: Bluetooth controller object.
 * @bdstr: Bluetooth address of destination device.
 * @clsid: SDP service class ID.
 * @err: An error message (if applicable).
 *
 * Performs a specific SDP scan for the service specified.  The service
 * class identifiers can be found in /usr/include/bluetooth/sdp.h
 *
 * Return value: %TRUE if the scan was successful.
 **/
gboolean
btctl_controller_scan_for_service(BtctlController *bc, const gchar *bdstr, guint clsid, GError **err)
{
	return btctl_controller_impl_scan_for_service(bc, bdstr, clsid, err);
}

/**
 * btctl_controller_discover_async:
 * @bc: Bluetooth controller object.
 *
 * Commence an asychronous device discovery cycle.  Signals will be sent
 * on device discovery, but no SDP discovery is being done.
 * The status-change signal will send information about the completion of the
 * scan.
 **/
void
btctl_controller_discover_async (BtctlController *bc)
{
	/* this discovery doesn't do any SDP, but asynchronously looks
	 * for devices and their names.  the BTCTL_STATUS_COMPLETE is
	 * sent when done
	 */
	btctl_discovery_source_send_inquiry (bc->source);
	btctl_controller_report_status(bc, BTCTL_STATUS_SCANNING);
}

/**
 * btctl_controller_is_initialised:
 * @bc: Bluetooth controller object.
 * @err: An error message (if applicable).
 *
 * Check if controller was able to get the Bluetooth HCI connection.
 * If not, we won't be able to do anything like discovery.
 *
 * Return value: TRUE if initialised OK.
 **/
gboolean
btctl_controller_is_initialised (BtctlController *bc, GError **err)
{
	g_return_val_if_fail (bc != NULL, FALSE);
	if (bc->init_err) {
		if (err)
			*err = g_error_copy(bc->init_err);
		return FALSE;
	}
	return btctl_discovery_source_is_initialised (bc->source);
}

/**
 * btctl_controller_get_signal_strength:
 * @bc: Bluetooth controller object.
 * @bdaddr: Destination device address.
 * @err: GError
 *
 * Bluetooth allows the monitoring of the signal strength of an
 * established connection.  There must be an existing connection for this
 * function to work.
 *
 * Return value: integer representing the signal strength.
 **/
int
btctl_controller_get_signal_strength (BtctlController *bc,
		const gchar *bdaddr, GError **err)
{
	return btctl_controller_impl_get_signal_strength (bc, bdaddr, err);
}

/**
 * btctl_controller_request_name:
 * @bc: Bluetooth controller object.
 * @bdaddr: Destination device address.
 *
 * Send a name request to the destination device. If and when it
 * responds, the result can be obtained from the "device-name" signal.
 *
 **/
void
btctl_controller_request_name (BtctlController *bc,
		const gchar *bdaddr)
{
	btctl_controller_impl_request_name (bc, bdaddr);
}

