/*
 * libbtctl -- GObject Bluetooth libraries
 * Copyright (C) 2003-2004 Edd Dumbill <edd@usefulinc.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

#include <stdio.h>
#include <stdlib.h>
#include <glib.h>
#include <glib-object.h>

#include "btctl.h"

static void status_callback(BtctlController *bc,
        gint    field,
        gpointer data) {

    printf("got status %d\n", field);
}

static void add_device_callback(BtctlController *bc,
        gchar*    name,
        gpointer data) {

    printf("got device %s\n", name);
}

static void device_name_callback(BtctlController *bc,
        gchar*   device,
        gchar*   name,
        gpointer data) {

    printf("device %s is called %s\n", device, name);
}

static void add_device_service_callback(BtctlController *bc,
                            gchar *addr, gchar *name, 
                            guint clsid, guint channel,
										gpointer data)
{
    printf("device %s (%s) has service %d channel %d\n",
                addr, name, clsid, channel);
}


int main(int argc, char **argv)
{
    BtctlController *bc;
    GError *err = NULL;
    
    g_type_init();

    bc=btctl_controller_new(NULL);
    g_signal_connect (G_OBJECT(bc), "status_change",
                G_CALLBACK(status_callback), NULL);
    g_signal_connect (G_OBJECT(bc), "add_device",
                G_CALLBACK(add_device_callback), NULL);
    g_signal_connect (G_OBJECT(bc), "device_name",
                G_CALLBACK(device_name_callback), NULL);
    g_signal_connect (G_OBJECT(bc), "add_device_service",
                G_CALLBACK(add_device_service_callback),
                NULL);

    if (btctl_controller_is_initialised (bc, &err) == FALSE)
    {
	printf ("No Bluetooth device initialised: %s\n",
		err ? err->message: "No Reason");
	exit (1);
    }

    /*
	printf("Try to get channel 1 of 00:80:37:2A:B6:BC ==> %d\n", 
		   btctl_controller_establish_rfcomm_connection(bc, "00:80:37:2A:B6:BC", 1));
           */

	btctl_controller_list_rfcomm_connections(bc);

	printf("Connection to 00:80:37:2A:B6:BC %d\n", 
		   btctl_controller_get_established_rfcomm_connection(bc, "00:80:37:2A:B6:BC", 0));

    btctl_controller_discover_devices(bc, &err);
    if (err)
    {
    	printf ("Device discovery failed: %s\n", err->message);
    	g_object_unref(bc);
    	return 1;
    }

    printf("Looking to see what channel OPUSH is on 00:80:37:2A:B6:BC\n");

    if (btctl_controller_scan_for_service(bc, "00:80:37:2A:B6:BC", 0x1105, &err) < 0)
    {
    	printf("Scanning for service failed: %s\n",
    		err ? err->message: "No Reason");
    	g_object_unref(bc);
    	return 1;
    }
    
    g_object_unref(bc);
    return 0;
}
