/*
 * libbtctl -- GObject Bluetooth libraries
 * Copyright (C) 2003-2004 Edd Dumbill <edd@usefulinc.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

#include <stdlib.h>
#include <stdio.h>
#include <getopt.h>
#include <errno.h>

#include <glib.h>

#include <bluetooth/sdp.h>
#include <bluetooth/sdp_lib.h>

#include "bthelper.h"

gchar *
bthelper_svc_id_to_class(guint svc)
{
	uuid_t uuid;
	gchar str[MAX_LEN_SERVICECLASS_UUID_STR];

	uuid.type=SDP_UUID16;
	uuid.value.uuid16=(uint16_t)svc;
	sdp_svclass_uuid2strn(&uuid, str, MAX_LEN_SERVICECLASS_UUID_STR);
	return g_strdup(str);
}
