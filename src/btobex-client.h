/*
 *  OBEX Client GObject.
 *
 *  Copyright (C) 2004  Edd Dumbill <edd@usefulinc.com>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#ifndef __BTOBEXCLIENT_H__
#define __BTOBEXCLIENT_H__

#include <glib-object.h>

#include "btobex.h"
#include "obex-client-source.h"

G_BEGIN_DECLS

#define BTCTL_TYPE_OBEX_CLIENT          (btctl_obex_client_get_type())
#define BTCTL_OBEX_CLIENT(obj)          (G_TYPE_CHECK_INSTANCE_CAST (obj, BTCTL_TYPE_OBEX_CLIENT, BtctlObexClient))
#define BTCTL_OBEX_CLIENT_CLASS(klass)  (G_TYPE_CHECK_CLASS_CAST (klass, BTCTL_TYPE_OBEX_CLIENT, BtctlObexClientClass))
#define BTCTL_IS_OBEX_CLIENT(obj)       (G_TYPE_CHECK_INSTANCE_TYPE (obj, BTCTL_TYPE_OBEX_CLIENT))
#define BTCTL_IS_OBEX_CLIENT_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE ((klass), BTCTL_TYPE_OBEX_CLIENT))
#define BTCTL_OBEX_CLIENT_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS ((obj), BTCTL_TYPE_OBEX_CLIENT, BtctlObexClientClass)


typedef struct _BtctlObexClient BtctlObexClient;
typedef struct _BtctlObexClientClass BtctlObexClientClass;

struct _BtctlObexClient
{
    GObject obj;
    BtctlObexclientSource *source;
    gboolean response;
    gchar *bdaddr;
    guint channel;
};

struct _BtctlObexClientClass
{
  GObjectClass	parent_class;
  void (* progress) (BtctlObexClient *bc, const gchar *bdaddr);
  void (* complete) (BtctlObexClient *bc, const gchar *bdaddr);
  void (* error) (BtctlObexClient *bc, const gchar *bdaddr, guint code);
  void (* connected) (BtctlObexClient *bc, const gchar *bdaddr);
  void (* disconnected) (BtctlObexClient *bc, const gchar *bdaddr);
};

/* Gtk housekeeping methods */

GType	btctl_obex_client_get_type	(void);
BtctlObexClient* btctl_obex_client_new	(void);

/* public methods */

BtctlObexClient* btctl_obex_client_new_and_connect (gchar *bdaddr, guint channel);
gboolean btctl_obex_client_disconnect (BtctlObexClient *bo);
gboolean btctl_obex_client_is_initialised (BtctlObexClient *bo);
void btctl_obex_client_set_response (BtctlObexClient *bo, gboolean resp);
gboolean btctl_obex_client_push_data (BtctlObexClient *bo, gchar *fname, const guchar * data, guint len);
gdouble btctl_obex_client_progress (BtctlObexClient *bo);

/* error codes */

#define BTCTL_OBEX_CLIENT_ERR_PARSE    0x01
#define BTCTL_OBEX_CLIENT_ERR_LINK     0x02
#define BTCTL_OBEX_CLIENT_ERR_ABORT    0x03

G_END_DECLS

#endif /* __BTOBEX_CLIENT_H__ */
