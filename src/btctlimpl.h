#ifndef _BTCTL_IMPL
#define _BTCTL_IMPL

#include <libintl.h>
#define _(x) dgettext(GETTEXT_PACKAGE, x)

#include "btctl.h"
#include <bluetooth/bluetooth.h>
#include <bluetooth/hci.h>

struct _BtctlDiscoverySource {
    GSource gsource;
    GPollFD fd;
    guchar buf[HCI_MAX_EVENT_SIZE];
    gint len;
    gboolean initialised;
};

void btctl_controller_report_status(BtctlController *bc, gint status);
void btctl_controller_got_device(BtctlController *bc, gchar *devaddr, guint clsid);
void btctl_controller_got_device_name(BtctlController *bc, gchar *devaddr, gchar *name);
void btctl_controller_got_device_service(BtctlController *bc, gchar *devaddr,
			      gchar *name, guint classid, guint port);
void btctl_controller_impl_cmd_scan(BtctlController *bc, GError **err);
int btctl_controller_impl_init(void);
void btctl_controller_impl_free(BtctlController *bc);
void btctl_controller_impl_list_rfcomm_connections(BtctlController *bc);
gint btctl_controller_impl_get_established_rfcomm_connection(BtctlController *bc, const gchar *bdstr, 
												 guint channel);
gint btctl_controller_impl_establish_rfcomm_connection(BtctlController *bc, const gchar *bdstr, 
										   guint channel);
gint btctl_controller_impl_scan_for_service(BtctlController *bc, const gchar *bdstr, guint clsid, GError **err);
int btctl_controller_impl_get_signal_strength (BtctlController *bc, 		const gchar *bdstr, GError **err);
void btctl_controller_impl_request_name (BtctlController *bc, const gchar *bdaddr);

gboolean btctl_controller_impl_get_discoverable(BtctlController *bc, GError **err);
void btctl_controller_impl_set_discoverable(BtctlController *bc, gboolean discoverable, GError **err);

void btctl_controller_impl_set_interface(BtctlController *bc, const char *hci_device, GError **err);

void btctl_controller_impl_init_source(BtctlController *bc);


#endif
