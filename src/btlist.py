#!/usr/bin/python

import sys
import btctl

def status_callback (bt, field):
	print ("got status %d" % (field))

def add_device_callback (bt, name, clsid):
	print ("got device %s (%d)" % (name, clsid))

def device_name_callback (bt, device, name):
	print ("device %s is called %s" % (device, name))

def add_device_service_callback (bt, addr, name, clsid, channel):
	print ("device %s (%s) has service %d channel %d" % (addr, name, clsid, channel))

bt = btctl.Controller()
bt.connect("status_change", status_callback);
bt.connect("add_device", add_device_callback);
bt.connect("device_name", device_name_callback);
bt.connect("add_device_service", add_device_service_callback);

if not bt.is_initialised():
  print ("No device to initialise")
  sys.exit (1)

bt.list_rfcomm_connections()
bt.discover_devices();

