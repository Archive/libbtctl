/*
 * libbtctl -- GObject Bluetooth libraries
 * Copyright (C) 2005 Jorge Perez Burgos <jperez@adaptia.es>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

#include <string.h>
#include "btctl-types.h"

static gpointer
btctl_obex_data_copy (gpointer data)
{
	g_return_val_if_fail (data != NULL, NULL);

	return g_memdup(data, sizeof(BtctlObexData));
}

static void
btctl_obex_data_free (gpointer data)
{
	g_return_if_fail(data);

	g_free(data);
}

GType
btctl_obex_data_get_type (void)
{
	static GType type = 0;
	if (!type)
		type = g_boxed_type_register_static (
			"BtctlObexData",
			btctl_obex_data_copy, btctl_obex_data_free);
	return type;
}
