/*
 *  GSource source for asynchronous HCI operations
 *
 *  Copyright (C) 2003  Edd Dumbill <edd@usefulinc.com>
 *  Copyright (C) 2003  Marcel Holtmann <marcel@holtmann.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <signal.h>
#include <sys/poll.h>
#include <sys/socket.h>

#include <bluetooth/bluetooth.h>
#include <bluetooth/hci.h>
#include <bluetooth/hci_lib.h>

#include <glib.h>

#include "btctlimpl.h"
#include "btctl-discovery-source.h"

/* GSourceFuncs */

static gboolean btctl_discovery_source_prepare (GSource *source, gint *timeout);
static gboolean btctl_discovery_source_check (GSource *source);
static gboolean btctl_discovery_source_dispatch (GSource *source,
        BtctlDiscoverySourceFunc callback, gpointer user_data);
static void btctl_discovery_source_finalize (GSource *source);

static GSourceFuncs btd_source = {
    btctl_discovery_source_prepare,
    btctl_discovery_source_check,
    (gboolean (*)(GSource *, GSourceFunc, gpointer))
        btctl_discovery_source_dispatch,
    btctl_discovery_source_finalize,
    NULL,
    NULL
};

void 
btctl_discovery_source_send_inquiry (BtctlDiscoverySource *source)
{
    inquiry_cp cp;
    int err;
    int dd;

    if (!source->initialised)
        return;

    dd = source->fd.fd;

    memset (&cp, 0, sizeof(cp));
    cp.lap[2] = 0x9e;
    cp.lap[1] = 0x8b;
    cp.lap[0] = 0x33;
    cp.length = 0x10;
    cp.num_rsp = 0;
    err = hci_send_cmd (dd, OGF_LINK_CTL, OCF_INQUIRY,
            INQUIRY_CP_SIZE, &cp);
}

void 
btctl_discovery_source_cancel_inquiry (BtctlDiscoverySource *source)
{   
    int err;
    int dd;

    if (!source->initialised)
        return;

    dd = source->fd.fd;

    err = hci_send_cmd (dd, OGF_LINK_CTL, OCF_INQUIRY_CANCEL,
            0, NULL);
}

static gboolean
btctl_discovery_source_prepare (GSource *source, gint *timeout)
{
    return FALSE;
}

static gboolean
btctl_discovery_source_check (GSource *source)
{
    BtctlDiscoverySource *s = (BtctlDiscoverySource*)source;

    if (!s->initialised)
        return FALSE;

    s->len = read(s->fd.fd, s->buf, HCI_MAX_EVENT_SIZE);
    // g_message("Got %d bytes", s->len);
    if (s->len < 0)
        return FALSE;

    return TRUE;
}

static gboolean
btctl_discovery_source_dispatch (GSource *source,
        BtctlDiscoverySourceFunc callback,
        gpointer user_data)
{
    BtctlDiscoverySource *s = (BtctlDiscoverySource*)source;

    if (callback) {
        (*callback)(s, user_data);
    }
    return TRUE;
}

static void
btctl_discovery_source_finalize (GSource *source)
{
    BtctlDiscoverySource *s = (BtctlDiscoverySource*)source;

    if (!s->initialised)
        return;

	if (hci_close_dev(s->fd.fd) < 0) {
		g_error("Can't close HCI device");
	}
}

void
btctl_discovery_source_destroy (BtctlDiscoverySource *source)
{
    g_source_destroy ((GSource *)source);
}

void
btctl_discovery_source_attach (BtctlDiscoverySource *source,
        GMainContext *ctxt)
{
    g_source_attach ((GSource *)source, ctxt);
}

BtctlDiscoverySource *
btctl_discovery_source_new ()
{
    BtctlDiscoverySource *source;
	struct hci_filter flt;
	int dev = 0;
    int fd;

    source = (BtctlDiscoverySource*) g_source_new (
		    &btd_source, sizeof(BtctlDiscoverySource));
    if (!source) {
	    g_assert_not_reached();
	    return NULL;
    }

    source->initialised = TRUE;

    //FIXME do we want to try that on all the HCI devices on the system?
	fd = hci_open_dev(dev);
    fcntl (fd, F_SETFL, O_NONBLOCK);
	if (fd < 0) {
		source->initialised = FALSE;
		return source;
	}

	hci_filter_clear(&flt);
	hci_filter_set_ptype(HCI_EVENT_PKT, &flt);
	hci_filter_set_event(EVT_INQUIRY_RESULT, &flt);
	hci_filter_set_event(EVT_INQUIRY_RESULT_WITH_RSSI, &flt);
	hci_filter_set_event(EVT_INQUIRY_COMPLETE, &flt);
	hci_filter_set_event(EVT_CONN_REQUEST, &flt);
	hci_filter_set_event(EVT_CONN_COMPLETE, &flt);
	hci_filter_set_event(EVT_REMOTE_NAME_REQ_COMPLETE, &flt);

	if (setsockopt(fd, SOL_HCI, HCI_FILTER, &flt, sizeof(flt)) < 0) {
		source->initialised = FALSE;
        hci_close_dev(fd);
		return source;
	}

    source->fd.fd = fd;
    source->fd.events = G_IO_IN | G_IO_HUP | G_IO_ERR;
    source->fd.revents = 0;

    g_source_add_poll ((GSource*) source, &source->fd);

    return source;
}

void
btctl_discovery_source_set_callback (BtctlDiscoverySource *source,
        BtctlDiscoverySourceFunc func, gpointer data,
        GDestroyNotify notify)
{
    g_source_set_callback ((GSource*)source,
            (GSourceFunc)func, data, notify);
}

gboolean
btctl_discovery_source_is_initialised (BtctlDiscoverySource *source)
{
    return source->initialised;
}

