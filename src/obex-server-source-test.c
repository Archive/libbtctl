/*
 * libbtctl -- GObject Bluetooth libraries
 * Copyright (C) 2003-2004 Edd Dumbill <edd@usefulinc.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */
#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <signal.h>
#include <sys/poll.h>
#include <sys/socket.h>

#include <bluetooth/bluetooth.h>
#include <bluetooth/hci.h>
#include <bluetooth/hci_lib.h>

#include <glib.h>

#include "obex-server-source.h"

/* main callback handler */

static gboolean
dohint (BtctlObexserverSource *src, BtctlObexserverEvent *evt, gpointer data)
{
    return TRUE;
}

static gboolean
doreq (BtctlObexserverSource *src, BtctlObexserverEvent *evt, gpointer data)
{
    return TRUE;
}

static gboolean
mycb (BtctlObexserverSource *src, BtctlObexserverEvent *evt, gpointer data)
{
    g_message ("MYCB event %d command %d", evt->event, evt->cmd);

    switch (evt->event) {
        case OBEX_EV_REQHINT:
            g_message ("## Request hint");
            return dohint (src, evt, data);
            break;
        case OBEX_EV_REQ:
            g_message ("## Request");
            return doreq (src, evt, data);
        case OBEX_EV_PARSEERR:
        case OBEX_EV_ABORT:
        case OBEX_EV_LINKERR:
            g_message ("## Transmission error");
            break;
        case OBEX_EV_REQDONE:
            g_message ("## Request complete");
            break;
        case OBEX_EV_PROGRESS:
            g_message ("## Progress");
            break;
        default:
            break;
    }
    return TRUE;
}

int
main(int argc, char *argv[])
{
    GMainLoop *loop;
    BtctlObexserverSource *gobsrc;

    printf("Ensure channel 4 is registered as OPUSH with sdptool\n");
    printf("Then beam files at me.\n");
    printf("Hit Ctrl-C to quit.\n");

    loop = g_main_loop_new (NULL, FALSE);

    gobsrc = btctl_obexserver_source_new ();

    if (gobsrc) {

        btctl_obexserver_source_attach (gobsrc, NULL);

        btctl_obexserver_source_set_callback (gobsrc, mycb, NULL, NULL);

        /*
        g_timeout_add (10*1000, (GSourceFunc)g_main_loop_quit,
        (gpointer)loop);
        */

        g_main_loop_run (loop);

        btctl_obexserver_source_destroy (gobsrc);
        btctl_obexserver_source_unref (gobsrc);

        g_main_loop_unref (loop);
    } else {
        g_error ("couldn't create source");
    }

	return 0;
}

