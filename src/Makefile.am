## Process this file with automake to produce Makefile.in

AM_CPPFLAGS = \
	-g \
	-I$(top_srcdir)/intl \
	$(BTCTL_CFLAGS) \
	$(BLUETOOTH_CFLAGS) \
	$(BTCTLPY_CFLAGS) \
	$(PYTHON_INCLUDES) \
	$(OPENOBEX_CFLAGS) \
	-DDATA_DIR="\"$(BTCTL_DATA_DIR)\"" \
	-DGETTEXT_PACKAGE="\"$(GETTEXT_PACKAGE)\""

EXTRA_DIST = \
	btctl-marshal.list

lib_LTLIBRARIES = libbtctl.la
if HAVE_PYTHON
pydir = $(PYTHON_PREFIX)/lib/python$(PYTHON_VERSION)/site-packages/
py_LTLIBRARIES = btctl.la
else
pydir =
py_LTLIBRARIES =
endif

$(libbtctl_la_OBJECTS): btctl-marshal.h

noinst_PROGRAMS=btlist btctl-discovery-test btctl-async-test btsignal-watch \
		obex-server-source-test btobex-test obex-client-source-test \
		btobexclient-test

obex_client_source_test_LDADD = \
	libbtctl.la \
	$(BTCTL_LIBS) $(BLUETOOTH_LIBS) $(OPENOBEX_LIBS)

obex_server_source_test_LDADD = \
	libbtctl.la \
	$(BTCTL_LIBS) $(BLUETOOTH_LIBS) $(OPENOBEX_LIBS)

btctl_async_test_LDADD = \
	libbtctl.la  $(BTCTL_LIBS) \
	$(BLUETOOTH_LIBS) $(OPENOBEX_LIBS)

btobexclient_test_LDADD = \
	libbtctl.la  $(BTCTL_LIBS) \
	$(BLUETOOTH_LIBS) $(OPENOBEX_LIBS)

btobex_test_LDADD = \
	libbtctl.la  $(BTCTL_LIBS) \
	$(BLUETOOTH_LIBS) $(OPENOBEX_LIBS)

btctl_discovery_test_LDADD = \
	libbtctl.la  $(BTCTL_LIBS) \
	$(BLUETOOTH_LIBS) $(OPENOBEX_LIBS)

btlist_LDADD = \
	libbtctl.la  $(BTCTL_LIBS) \
	$(BLUETOOTH_LIBS) $(OPENOBEX_LIBS)

btsignal_watch_LDADD = \
	libbtctl.la $(BTCTL_LIBS) \
	$(BLUETOOTH_LIBS) $(OPENOBEX_LIBS)

libbtctl_la_SOURCES = \
	btctl.c btctl.h \
	btctlimpl.c btctlimpl.h \
	btctl-marshal.c btctl-marshal.h \
	bthelper.c bthelper.h \
	btctl-discovery-source.c btctl-discovery-source.h \
	obex-server-source.c obex-server-source.h \
	obex-server-source-private.h \
	btobex.c btobex.h obexsdp.h obexsdp.c \
	obex-client-source.c obex-client-source.h \
	btobex-client.c btobex-client.h \
	btctl-types.h btctl-types.c

pkginclude_HEADERS = \
	btctl.h bthelper.h btctl-discovery-source.h \
	obex-server-source.h btobex.h obex-client-source.h \
	btobex-client.h btctl-types.h

libbtctl_la_LDFLAGS = \
	-version-info $(BTCTL_LT_VERSION)
libbtctl_la_LIBADD = \
	$(BTCTL_LIBS) $(BLUETOOTH_LIBS) $(OPENOBEX_LIBS)



GENMARSHAL_GENERATED = btctl-marshal.h btctl-marshal.c

btctl.c: $(GENMARSHAL_GENERATED)

btctl-marshal.h: $(srcdir)/btctl-marshal.list
	( @GLIB_GENMARSHAL@ --prefix=btctl_marshal $(srcdir)/btctl-marshal.list --header > btctl-marshal.tmp \
	&& mv btctl-marshal.tmp btctl-marshal.h ) \
	|| ( rm -f btctl-marshal.tmp && exit 1 )

btctl-marshal.c: btctl-marshal.h
	( echo "#include \"btctl-marshal.h\"" >btctl-marshal.tmp \
	&& @GLIB_GENMARSHAL@ --prefix=btctl_marshal $(srcdir)/btctl-marshal.list --body >> btctl-marshal.tmp \
	&& mv btctl-marshal.tmp btctl-marshal.c ) \
	|| ( rm -f btctl-marshal.tmp && exit 1 )

BUILT_SOURCES = $(GENMARSHAL_GENERATED)

CLEANFILES = $(GENMARSHAL_GENERATED)

dist-hook:
	cd $(distdir); rm -f $(BUILT_SOURCES)

# Python bindings
CLEANFILES += btctl-py.c
EXTRA_DIST += btctl.defs btctl.override

if HAVE_PYTHON
regenerate-defs: btctl.h btobex.h btobex-client.h
	python $(CODEGENDIR)/h2def.py -v btctl.h btobex.h btobex-client.h > btctl.defs

btctl-py.c: btctl.defs btctl.override
	pygtk-codegen-2.0 --prefix btctl \
	--register $(DEFSDIR)/gtk-types.defs \
	--override btctl.override \
	btctl.defs > $@

btctl_la_SOURCES = btctl-py.c btctl-pymodule.c
btctl_la_LIBADD = libbtctl.la
btctl_la_LDFLAGS = -module -avoid-version
btctl_la_LIBADD = $(BTCTLPY_LIBS)
endif

