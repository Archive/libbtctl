/*
 * libbtctl -- GObject Bluetooth libraries
 * Copyright (C) 2003-2004 Edd Dumbill <edd@usefulinc.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */
#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <signal.h>
#include <sys/poll.h>
#include <sys/socket.h>

#include <bluetooth/bluetooth.h>
#include <bluetooth/hci.h>
#include <bluetooth/hci_lib.h>

#include <glib.h>

#include "btctlimpl.h"
#include "btctl-discovery-source.h"

/* main callback handler */

static gboolean cb_hci_data (BtctlDiscoverySource *source, gpointer user_data);

/* subhandlers */

static void name_request (int dd, bdaddr_t bdaddr);
static void inquiry_result(int dd, unsigned char *buf, int len);
static void inquiry_result_with_rssi(int dd, unsigned char *buf, int len);
static void conn_request(int dd, unsigned char *buf, int len);
static void conn_complete(int dd, unsigned char *buf, int len);
static void name_complete(int dd, unsigned char *buf, int len);
static void inquiry_complete (int dd);

static void
inquiry_result(int dd, unsigned char *buf, int len)
{
	inquiry_info *info;
	uint8_t num;
	char addr[18];
	int i;
	uint32_t devclass;

	num = buf[0];
	printf("inquiry_result:\tnum %d\n", num);

	for (i = 0; i < num; i++) {
		info = (void *) (buf + (sizeof(*info) * i) + 1);
		ba2str(&info->bdaddr, addr);
        devclass = (uint32_t)(info->dev_class[2]) << 16 |
            (uint32_t)(info->dev_class[1]) << 8 |
            (uint32_t)(info->dev_class[0]);
		printf("inquiry_result:\tbdaddr %s class %x\n", addr, devclass);
	}
}

static void
inquiry_result_with_rssi(int dd, unsigned char *buf, int len)
{
	inquiry_info_with_rssi *info;
	uint8_t num;
	char addr[18];
	int i;
	uint32_t devclass;

	num = buf[0];
	printf("inquiry_result_with_rssi:\tnum %d\n", num);

	for (i = 0; i < num; i++) {
		info = (void *) (buf + (sizeof(*info) * i) + 1);
		ba2str(&info->bdaddr, addr);
        devclass = (uint32_t)(info->dev_class[2]) << 16 |
            (uint32_t)(info->dev_class[1]) << 8 |
            (uint32_t)(info->dev_class[0]);
		printf("inquiry_result_with_rssi:\tbdaddr %s class %x\n", addr, devclass);
	}
}

static void
conn_request(int dd, unsigned char *buf, int len)
{
	evt_conn_request *evt = (void *) buf;
	remote_name_req_cp cp;
	char addr[18];

	ba2str(&evt->bdaddr, addr);
	printf("conn_request:\tbdaddr %s\n", addr);

	memset(&cp, 0, sizeof(cp));
	bacpy(&cp.bdaddr, &evt->bdaddr);
	cp.pscan_rep_mode = 0x01;
	cp.pscan_mode     = 0x00;
	cp.clock_offset   = 0x0000;

	//hci_send_cmd(dd, OGF_LINK_CTL, OCF_REMOTE_NAME_REQ,
	//			REMOTE_NAME_REQ_CP_SIZE, &cp);
}

static void
name_request (int dd, bdaddr_t bdaddr)
{
	remote_name_req_cp cp;

	memset(&cp, 0, sizeof(cp));
	bacpy(&cp.bdaddr, &bdaddr);
	cp.pscan_rep_mode = 0x01;
	cp.pscan_mode     = 0x00;
	cp.clock_offset   = 0x0000;

	hci_send_cmd(dd, OGF_LINK_CTL, OCF_REMOTE_NAME_REQ,
				REMOTE_NAME_REQ_CP_SIZE, &cp);
}

static void
conn_complete(int dd, unsigned char *buf, int len)
{
	evt_conn_complete *evt = (void *) buf;
	remote_name_req_cp cp;

	printf("conn_complete:\tstatus 0x%02x\n", evt->status);

	if (evt->status)
		return;

	memset(&cp, 0, sizeof(cp));
	bacpy(&cp.bdaddr, &evt->bdaddr);
	cp.pscan_rep_mode = 0x01;
	cp.pscan_mode     = 0x00;
	cp.clock_offset   = 0x0000;

	hci_send_cmd(dd, OGF_LINK_CTL, OCF_REMOTE_NAME_REQ,
				REMOTE_NAME_REQ_CP_SIZE, &cp);
	name_request (dd, evt->bdaddr);
}

static void
name_complete(int dd, unsigned char *buf, int len)
{
	evt_remote_name_req_complete *evt = (void *) buf;
	char addr[18];

	printf("name_complete:\tstatus 0x%02x\n", evt->status);

	if (evt->status)
		return;

	ba2str(&evt->bdaddr, addr);
	printf("name_complete:\tbdaddr %s %s\n", addr, evt->name);
}

static void
inquiry_complete (int dd)
{
	printf("inquiry complete\n");
}

static gboolean
cb_hci_data (BtctlDiscoverySource *source, gpointer user_data)
{
	hci_event_hdr *hdr;
	unsigned char *ptr;
	int len;

	hdr = (void *) (source->buf + 1);
	ptr = source->buf + (1 + HCI_EVENT_HDR_SIZE);
	len = source->len - (1 + HCI_EVENT_HDR_SIZE);

	switch (hdr->evt) {
	case EVT_INQUIRY_RESULT:
		inquiry_result(source->fd.fd, ptr, len);
		break;
	case EVT_INQUIRY_RESULT_WITH_RSSI:
		inquiry_result_with_rssi(source->fd.fd, ptr, len);
		break;
	case EVT_INQUIRY_COMPLETE:
		inquiry_complete(source->fd.fd);
		break;
	case EVT_CONN_REQUEST:
		conn_request(source->fd.fd, ptr, len);
		break;
	case EVT_CONN_COMPLETE:
		conn_complete(source->fd.fd, ptr, len);
		break;
	case EVT_REMOTE_NAME_REQ_COMPLETE:
		name_complete(source->fd.fd, ptr, len);
		break;
	}

	return TRUE;
}

int
main(int argc, char *argv[])
{
	BtctlDiscoverySource *btdsrc;
	GMainLoop *loop;

	printf("Sending out an HCI inquiry and listening for the results\n");
	printf("Hit Ctrl-C to quit\n");


	loop = g_main_loop_new (NULL, FALSE);
	btdsrc = btctl_discovery_source_new ();

	btctl_discovery_source_set_callback (btdsrc, cb_hci_data, NULL, NULL);
	btctl_discovery_source_send_inquiry (btdsrc);

	btctl_discovery_source_attach (btdsrc, NULL);

	g_timeout_add (20*1000, (GSourceFunc)g_main_loop_quit, (gpointer)loop);
	g_main_loop_run (loop);

	btctl_discovery_source_destroy (btdsrc);
	btctl_discovery_source_unref (btdsrc);

	g_main_loop_unref (loop);

	return 0;
}
