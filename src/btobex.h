/*
 *  OBEX GObject.
 *
 *  Copyright (C) 2004  Edd Dumbill <edd@usefulinc.com>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#ifndef __BTOBEX_H__
#define __BTOBEX_H__

#include <glib-object.h>

#include "obex-server-source.h"
#include "btctl-types.h"

G_BEGIN_DECLS

#define BTCTL_TYPE_OBEX          (btctl_obex_get_type())
#define BTCTL_OBEX(obj)          (G_TYPE_CHECK_INSTANCE_CAST (obj, BTCTL_TYPE_OBEX, BtctlObex))
#define BTCTL_OBEX_CLASS(klass)  (G_TYPE_CHECK_CLASS_CAST (klass, BTCTL_TYPE_OBEX, BtctlObexClass))
#define BTCTL_IS_OBEX(obj)       (G_TYPE_CHECK_INSTANCE_TYPE (obj, BTCTL_TYPE_OBEX))
#define BTCTL_IS_OBEX_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE ((klass), BTCTL_TYPE_OBEX))
#define BTCTL_OBEX_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS ((obj), BTCTL_TYPE_OBEX, BtctlObexClass)


typedef struct _BtctlObex BtctlObex;
typedef struct _BtctlObexClass BtctlObexClass;

struct _BtctlObex
{
    GObject obj;
    BtctlObexserverSource *source;
    gboolean response;
    guint32 sdphandle;
};

struct _BtctlObexClass
{
  GObjectClass	parent_class;
  void (* request_put) (BtctlObex *bc, const gchar *bdaddr);
  void (* put) (BtctlObex *bc, const gchar *bdaddr, const gchar *fname, BtctlObexData *data, guint timestamp);
  void (* progress) (BtctlObex *bc, const gchar *bdaddr);
  void (* complete) (BtctlObex *bc, const gchar *bdaddr);
  void (* error) (BtctlObex *bc, const gchar *bdaddr, guint code);
  void (* connect) (BtctlObex *bc, const gchar *bdaddr);
  void (* disconnect) (BtctlObex *bc, const gchar *bdaddr);
};

/* Gtk housekeeping methods */

GType	btctl_obex_get_type	(void);
BtctlObex* btctl_obex_new	(void);

/* public methods */

gboolean btctl_obex_is_initialised (BtctlObex *bo);
void btctl_obex_set_response (BtctlObex *bo, gboolean resp);
void btctl_obex_cancel_operation (BtctlObex *bo);
void btctl_obex_cancel_operation_forcibly (BtctlObex *bo);

/* error codes */

#define BTCTL_OBEX_ERR_PARSE    0x01
#define BTCTL_OBEX_ERR_LINK     0x02
#define BTCTL_OBEX_ERR_ABORT    0x03

G_END_DECLS

#endif /* __BTOBEX_H__ */
