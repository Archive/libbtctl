#!/usr/bin/python

import os
import sys
import btctl
import gtk

# example obex server in python

def progress_callback (bo, bdaddr):
    print ("got progress from %s" % (bdaddr))

def request_put_callback (bo, bdaddr):
    print ("device %s wants to send a file" % (bdaddr))
    bo.set_response (True)

def put_callback (bo, bdaddr, fname, body, timestamp):
    print ("device %s sent a file" % (bdaddr))
    print "Filename %s, timestamp %d " % (fname, timestamp)
    f = open("/tmp" + os.sep + fname,'wb')
    f.write(body)
    f.close()
    print "File saved in /tmp" + os.sep + fname
    bo.set_response (True)

def error_callback (bo, bdaddr, err):
    print ("got error %d from %s" % (err, bdaddr))

def complete_callback (bo, bdaddr):
    print ("transfer complete from %s" % (bdaddr))

bo = btctl.Obex()
bo.connect("progress", progress_callback);
bo.connect("request-put", request_put_callback);
bo.connect("put", put_callback);
bo.connect("error", error_callback);
bo.connect("complete", complete_callback);

if not bo.is_initialised():
  print ("Server not initialised")
  sys.exit (1)

print "Send me files. Hit Ctrl-C to terminate."
gtk.main ()
