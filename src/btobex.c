/*
 * libbtctl -- GObject Bluetooth libraries
 * Copyright (C) 2003-2004 Edd Dumbill <edd@usefulinc.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */
#define _GNU_SOURCE /* glibc2 needs this */
#include <config.h>

#include <stdio.h>
#include <glib.h>
#include <glib-object.h>
#include <assert.h>
#include <time.h>

#include "btobex.h"
#include "btctl-types.h"
#include "btctl-marshal.h"
#include "obexsdp.h"

static gpointer		 parent_class = NULL;

/* gtk object prototypes */

static void btctl_obex_class_init (BtctlObexClass *class);
static void btctl_obex_init (BtctlObex *bc);
static void btctl_obex_finalize(GObject *obj);

/* other static functions */
static gboolean
maincb (BtctlObexserverSource *src, BtctlObexserverEvent *evt, gpointer data);
static gboolean
reqcb (BtctlObexserverSource *src, BtctlObexserverEvent *evt, gpointer data);
static gboolean
hintcb (BtctlObexserverSource *src, BtctlObexserverEvent *evt, gpointer data);


/* gtk signal defns */

enum {
	REQUEST_PUT_SIGNAL,
	PUT_SIGNAL,
	PROGRESS_SIGNAL,
	COMPLETE_SIGNAL,
	ERROR_SIGNAL,
	CONNECT_SIGNAL,
	DISCONNECT_SIGNAL,
	LAST_SIGNAL
};

static gint btctl_obex_signals[LAST_SIGNAL] = { 0 } ;

/* BtctlObex functions */
G_DEFINE_TYPE(BtctlObex, btctl_obex, G_TYPE_OBJECT)

static void
btctl_obex_class_init (BtctlObexClass *klass)
{
	GObjectClass *object_class;

	parent_class = g_type_class_ref(G_TYPE_OBJECT);

	object_class=(GObjectClass*)klass;

	btctl_obex_signals[REQUEST_PUT_SIGNAL] =
		g_signal_new ("request-put",
			G_OBJECT_CLASS_TYPE(klass),
			G_SIGNAL_RUN_FIRST,
			G_STRUCT_OFFSET(BtctlObexClass, request_put),
			NULL /* accu */, NULL,
			btctl_marshal_VOID__STRING,
			G_TYPE_NONE,
			1,
			G_TYPE_STRING);

	btctl_obex_signals[PUT_SIGNAL] =
		g_signal_new ("put",
			G_OBJECT_CLASS_TYPE(klass),
			G_SIGNAL_RUN_FIRST,
			G_STRUCT_OFFSET(BtctlObexClass, put),
			NULL /* accu */, NULL,
			btctl_marshal_VOID__STRING_STRING_POINTER_UINT,
			G_TYPE_NONE,
			4,
			G_TYPE_STRING,
			G_TYPE_STRING,
			BTCTL_TYPE_OBEX_DATA,
			G_TYPE_UINT);

	btctl_obex_signals[PROGRESS_SIGNAL] =
		g_signal_new ("progress",
			G_OBJECT_CLASS_TYPE(klass),
			G_SIGNAL_RUN_FIRST,
			G_STRUCT_OFFSET(BtctlObexClass, progress),
			NULL /* accu */, NULL,
			btctl_marshal_VOID__STRING,
			G_TYPE_NONE, 1, G_TYPE_STRING);

	btctl_obex_signals[COMPLETE_SIGNAL] =
		g_signal_new ("complete",
			G_OBJECT_CLASS_TYPE(klass),
			G_SIGNAL_RUN_FIRST,
			G_STRUCT_OFFSET(BtctlObexClass, complete),
			NULL /* accu */, NULL,
			btctl_marshal_VOID__STRING,
			G_TYPE_NONE, 1, G_TYPE_STRING);

	btctl_obex_signals[ERROR_SIGNAL] =
		g_signal_new ("error",
			G_OBJECT_CLASS_TYPE(klass),
			G_SIGNAL_RUN_FIRST,
			G_STRUCT_OFFSET(BtctlObexClass, error),
			NULL /* accu */, NULL,
			btctl_marshal_VOID__STRING_UINT,
			G_TYPE_NONE, 2, G_TYPE_STRING, G_TYPE_UINT);

	btctl_obex_signals[CONNECT_SIGNAL] =
		g_signal_new ("connect",
			G_OBJECT_CLASS_TYPE(klass),
			G_SIGNAL_RUN_FIRST,
			G_STRUCT_OFFSET(BtctlObexClass, connect),
			NULL /* accu */, NULL,
			btctl_marshal_VOID__STRING,
			G_TYPE_NONE, 1, G_TYPE_STRING);

	btctl_obex_signals[DISCONNECT_SIGNAL] =
		g_signal_new ("disconnect",
			G_OBJECT_CLASS_TYPE(klass),
			G_SIGNAL_RUN_FIRST,
			G_STRUCT_OFFSET(BtctlObexClass, disconnect),
			NULL /* accu */, NULL,
			btctl_marshal_VOID__STRING,
			G_TYPE_NONE, 1, G_TYPE_STRING);

	object_class->finalize = btctl_obex_finalize;

	klass->request_put = NULL;
	klass->put = NULL;
	klass->progress = NULL;
	klass->complete = NULL;
	klass->error = NULL;
}

static void
btctl_obex_init (BtctlObex *bo)
{
	bo->source = btctl_obexserver_source_new ();
	if (bo->source && bo->source->initialised) {
		btctl_obexserver_source_attach (bo->source, NULL);
		btctl_obexserver_source_set_callback (bo->source, maincb,
						(gpointer) bo, NULL);
		if (register_sdp (&bo->sdphandle) < 0)
			g_warning ("Unable to register SDP record for OPUSH");
	} else {
		g_warning ("Unable to initialize OBEX source");
	}
}

/**
 * btctl_obex_new:
 *
 * Create a new Bluetooth OBEX object.  This will attempt to open
 * an OBEX server socket. Use btctl_obex_is_initialised() to check
 * whether this was successful.
 *
 * Return value: a pointer to the controller object.
 **/
BtctlObex*
btctl_obex_new(void)
{
	return BTCTL_OBEX (g_object_new (btctl_obex_get_type(), NULL));
}

static void
btctl_obex_finalize(GObject *obj)
{
	BtctlObex *bo;

	bo=BTCTL_OBEX(obj);

	/* custom finalize stuff goes here
	 */
	if (bo->source) {
		deregister_sdp (bo->sdphandle);
		btctl_obexserver_source_destroy (bo->source);
		btctl_obexserver_source_unref (bo->source);
	}
	/* call superclass' destructor */
	G_OBJECT_CLASS(parent_class)->finalize(obj);
}

static void
btctl_obex_emit_request_put (BtctlObex *bo, gchar *bdaddr)
{
	g_signal_emit (G_OBJECT (bo),
		btctl_obex_signals[REQUEST_PUT_SIGNAL],
		0, bdaddr);
}

static void
btctl_obex_emit_connect (BtctlObex *bo, gchar *bdaddr)
{
	g_signal_emit (G_OBJECT (bo),
		btctl_obex_signals[CONNECT_SIGNAL],
		0, bdaddr);
}

static void
btctl_obex_emit_disconnect (BtctlObex *bo, gchar *bdaddr)
{
	g_signal_emit (G_OBJECT (bo),
		btctl_obex_signals[CONNECT_SIGNAL],
		0, bdaddr);
}

static void
btctl_obex_emit_progress (BtctlObex *bo, gchar *bdaddr)
{
	g_signal_emit (G_OBJECT (bo),
		btctl_obex_signals[PROGRESS_SIGNAL],
		0, bdaddr);
}

static void
btctl_obex_emit_put (BtctlObex *bo, gchar *bdaddr, guchar *fname,
				BtctlObexData* data, guint timestamp)
{
	g_signal_emit (G_OBJECT (bo),
		btctl_obex_signals[PUT_SIGNAL],
		0, bdaddr, fname, data, timestamp);
}

static void
btctl_obex_emit_complete (BtctlObex *bo, gchar *bdaddr)
{
	g_signal_emit (G_OBJECT (bo),
		btctl_obex_signals[COMPLETE_SIGNAL],
		0, bdaddr);
}

static void
btctl_obex_emit_error (BtctlObex *bo, gchar *bdaddr, guint reason)
{
	g_signal_emit (G_OBJECT (bo),
		btctl_obex_signals[ERROR_SIGNAL],
		0, bdaddr, reason);
}

static void
put_done (BtctlObex *bo, BtctlObexserverSource *src, BtctlObexserverEvent *evt)
{
	obex_headerdata_t hv;
	guint8 hi;
	gint hlen;
	const guint8 *body = NULL;
	guint body_len = 0;
	gchar *name = NULL;
	gchar *namebuf = NULL;
	guint timestamp = 0;
	gchar *timebuf = NULL;
	gchar *timebuf_tz = NULL;
	struct tm tm;

	obex_object_t *obj = (obex_object_t *) evt->data;
	while (OBEX_ObjectGetNextHeader (src->handle, obj, &hi, &hv, &hlen)) {
		switch (hi) {
			case OBEX_HDR_BODY:
				body = hv.bs;
				body_len = (guint) hlen;
				break;
			case OBEX_HDR_TIME:
				/* Last modification time of the file (ISO-8601 timestamp).
				 * 
				 * NB: some systems (W2k?) might send the timestamp
				 * in Unicode. Should we detect and handle that? */
				if ((timebuf = g_new0 (gchar, hlen+1))) {
					strncpy (timebuf, hv.bs, hlen);
					timebuf_tz = (gchar*) strptime (timebuf, "%Y%m%dT%H%M%S", &tm);
					if (timebuf_tz) {
						if (timebuf_tz[0]=='Z') {
							/* timestamp given in UTC */
							timestamp = timegm(&tm);
						} else if (timebuf_tz[0]=='\0') {
							/* timestamp given in localtime */
							timestamp = mktime(&tm);
						}
					}
					g_free (timebuf);
				}
				break;
			case OBEX_HDR_TIME2:
				/* NOTE: deprecated header, for compatibility only.
				 * This is ignored if OBEX_HDR_TIME is also given. */
				if (!timestamp)
					timestamp = hv.bq4;
				break;
			case OBEX_HDR_NAME:
				if ((namebuf = g_new0 (gchar, hlen/2))) {
					/* FIXME: figure out which encoding of unicode is
					 * being used and handle it properly */
					OBEX_UnicodeToChar (namebuf, hv.bs, hlen);
					name = namebuf;
				}
				break;
		}
	}
	if (!name) {
		name = g_strdup ("Unknown object");
	}
	if (body) {
		BtctlObexData *obexData = g_new0(BtctlObexData, 1);
		obexData->body = body;
		obexData->body_len = body_len;

		btctl_obex_emit_put (bo, src->peer_name, name, obexData, timestamp);
	} else {
		g_warning ("btctl_obex::put_done: no body received, ignoring");
	}
	g_free (name);
}

static gboolean
hintcb (BtctlObexserverSource *src, BtctlObexserverEvent *evt, gpointer data)
{
	BtctlObex *bo = BTCTL_OBEX (data);

	/* FIXME: default response is permissive */
	bo->response = TRUE;

	switch (evt->cmd) {
		case OBEX_CMD_PUT:
			btctl_obex_emit_request_put (bo, src->peer_name);
			break;
		default:
		   break;
	}
	
	return bo->response;
}

static gboolean
reqcb (BtctlObexserverSource *src, BtctlObexserverEvent *evt, gpointer data)
{
	BtctlObex *bo = BTCTL_OBEX (data);

	/* default response is permissive */
	bo->response = TRUE;

	switch (evt->cmd) {
		case OBEX_CMD_PUT:
			put_done (bo, src, evt);
			break;
		case OBEX_CMD_CONNECT:
			btctl_obex_emit_connect (bo, src->peer_name);
			break;
		case OBEX_CMD_DISCONNECT:
			btctl_obex_emit_disconnect (bo, src->peer_name);
			break;
		default:
			bo->response = FALSE;
			break;
	}

	return bo->response;
}

static gboolean
maincb (BtctlObexserverSource *src, BtctlObexserverEvent *evt, gpointer data)
{
	BtctlObex *bo = BTCTL_OBEX (data);
	/* g_message ("maincb event %d command %d", evt->event, evt->cmd); */

	switch (evt->event) {
		case OBEX_EV_REQHINT:
			return hintcb (src, evt, data);
			break;
		case OBEX_EV_REQ:
			return reqcb (src, evt, data);
		case OBEX_EV_PARSEERR:
			btctl_obex_emit_error (bo, src->peer_name,
					BTCTL_OBEX_ERR_PARSE);
			break;
		case OBEX_EV_ABORT:
			btctl_obex_emit_error (bo, src->peer_name,
					BTCTL_OBEX_ERR_ABORT);
			break;
		case OBEX_EV_LINKERR:
			btctl_obex_emit_error (bo, src->peer_name,
					BTCTL_OBEX_ERR_LINK);
			break;
		case OBEX_EV_REQDONE:
			btctl_obex_emit_complete (bo, src->peer_name);
			break;
		case OBEX_EV_PROGRESS:
			btctl_obex_emit_progress (bo, src->peer_name);
			break;
	}
	return TRUE;
}


/* substantial public methods */

/**
 * btctl_obex_is_initialised:
 * @bo: Bluetooth OBEX object.
 *
 * Check if OBEX was able to initialise OK.
 * If not, we won't be able to do anything.
 *
 * Return value: TRUE if initialised OK.
 **/
gboolean
btctl_obex_is_initialised (BtctlObex *bo)
{
	g_return_val_if_fail (bo != NULL, FALSE);
	return bo->source && bo->source->initialised;
}

/**
 * btctl_obex_set_response:
 * @bo: Bluetooth OBEX object.
 * @resp: Boolean response.
 *
 * Sets TRUE or FALSE response in signal handlers
 * that must decide whether an operation can proceed
 * or not.
 **/
void
btctl_obex_set_response (BtctlObex *bo, gboolean resp)
{
	bo->response = resp;
}

/**
 * btctl_obex_cancel_operation:
 * @bo: Bluetooth OBEX object.
 *
 * Requests the cancel of a currently running transfer,
 * disconnecting the client.
 **/
void
btctl_obex_cancel_operation (BtctlObex *bo)
{
	btctl_obexserver_source_client_cancel (bo->source);
}

/**
 * btctl_obex_cancel_operation_forcibly:
 * @bo: Bluetooth OBEX object.
 *
 * Forcibly causes the closing of a client connection.  Use
 * if the client isn't responding to a cancel request, e.g.
 * where the connection's timing out.
 **/
void
btctl_obex_cancel_operation_forcibly (BtctlObex *bo)
{
	btctl_obexserver_source_client_close (bo->source);
}

