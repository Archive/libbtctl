/*
 * libbtctl -- GObject Bluetooth libraries
 * Copyright (C) 2003-2004 Edd Dumbill <edd@usefulinc.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

#include <glib.h>
#include <glib-object.h>
#include <unistd.h>

#include "btctl.h"


int main (int argc, char **argv)
{
	BtctlController* bt;
	GError *error = NULL;

	g_type_init ();

	if (argc < 2)
	{
		g_print ("%s <device address>\n", argv[0]);
		return 0;
	}

	bt = btctl_controller_new (NULL);
	while (1) {
		int ret;

		ret = btctl_controller_get_signal_strength (bt, argv[1], &error);
		if (error != NULL)
		{
			g_print ("Error getting the signal strength for device %s.\nReason: %s.\n", argv[1], error->message);
			return 0;
		}

		g_print ("Strength for %s: %d\n", argv[1], ret);

		usleep (1000);
	}

	return 0;
}

