/*
 * libbtctl -- GObject Bluetooth libraries
 * Copyright (C) 2003-2004 Edd Dumbill <edd@usefulinc.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <signal.h>
#include <sys/poll.h>
#include <sys/socket.h>

#include <bluetooth/bluetooth.h>
#include <bluetooth/hci.h>
#include <bluetooth/hci_lib.h>

#include <glib.h>

#include "obex-client-source.h"

/* main callback handler */

static char *hello = "Hello, World!\n";

GMainLoop *loop;

static gboolean
dohint (BtctlObexclientSource *src, BtctlObexclientEvent *evt, gpointer data)
{
    return TRUE;
}

static gboolean
doreq (BtctlObexclientSource *src, BtctlObexclientEvent *evt, gpointer data)
{
    return TRUE;
}

static gboolean
disconnect_me (BtctlObexclientSource *src)
{
	g_message ("requesting disconnect");
	btctl_obexclient_source_disconnect (src);
	return FALSE;
}

static gboolean
send_data (BtctlObexclientSource *src)
{
	g_message ("sending some data");
	btctl_obexclient_source_push (src, "hello.txt", hello, strlen(hello));
	return FALSE;
}

static gboolean
mycb (BtctlObexclientSource *src, BtctlObexclientEvent *evt, gpointer data)
{
    g_message ("MYCB event %d command %d", evt->event, evt->cmd);

    switch (evt->event) {
        case OBEX_EV_REQHINT:
            g_message ("## Request hint");
            return dohint (src, evt, data);
            break;
        case OBEX_EV_REQ:
            g_message ("## Request");
            return doreq (src, evt, data);
        case OBEX_EV_PARSEERR:
        case OBEX_EV_ABORT:
        case OBEX_EV_LINKERR:
            g_message ("## Transmission error");
            break;
        case OBEX_EV_REQDONE:
            g_message ("## Request complete");
			switch (evt->cmd) {
				case OBEX_CMD_CONNECT:
					g_message ("Connect complete");
					g_idle_add ((GSourceFunc)send_data, (gpointer)src);
					break;
				case OBEX_CMD_DISCONNECT:
					g_message ("Disonnect complete");
					g_main_loop_quit (loop);
					break;
				case OBEX_CMD_PUT:
					g_message ("Put complete");
					g_idle_add ((GSourceFunc)disconnect_me, (gpointer)src);
					break;
			}
            break;
        case OBEX_EV_PROGRESS:
            g_message ("## Progress");
            break;
        default:
            break;
    }
    return TRUE;
}

int
main(int argc, char *argv[])
{
    BtctlObexclientSource *gobsrc;

    loop = g_main_loop_new (NULL, FALSE);

    gobsrc = btctl_obexclient_source_new ("08:00:46:CA:1B:CA", 4);

    if (gobsrc) {

        btctl_obexclient_source_attach (gobsrc, NULL);

        btctl_obexclient_source_set_callback (gobsrc, mycb, NULL, NULL);

        /*
        g_timeout_add (10*1000, (GSourceFunc)g_main_loop_quit,
        (gpointer)loop);
        */

		btctl_obexclient_source_connect (gobsrc);

        g_main_loop_run (loop);

        btctl_obexclient_source_destroy (gobsrc);
        btctl_obexclient_source_unref (gobsrc);

        g_main_loop_unref (loop);
    } else {
        g_error ("couldn't create source");
    }

	return 0;
}

