#ifndef __BTCTL_TYPES_H__
#define __BTCTL_TYPES_H__

#include <glib.h>
#include <glib-object.h>

G_BEGIN_DECLS

GType btctl_obex_data_get_type     (void) G_GNUC_CONST;
#define BTCTL_TYPE_OBEX_DATA       (btctl_obex_data_get_type ())


typedef struct _BtctlObexData BtctlObexData;
typedef BtctlObexData* GBtctlObexData;

struct _BtctlObexData
{
	const guint8 *body;
	guint body_len;
};

G_END_DECLS

#endif /* __BTCTL_TYPES_H__ */
