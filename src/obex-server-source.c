/*
 * libbtctl -- GObject Bluetooth libraries
 * Copyright (C) 2003-2004 Edd Dumbill <edd@usefulinc.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */
#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <signal.h>
#include <sys/poll.h>
#include <sys/socket.h>

#include <bluetooth/bluetooth.h>
#include <bluetooth/hci.h>
#include <bluetooth/hci_lib.h>
#include <bluetooth/rfcomm.h>

#include <glib.h>

#include "obex-server-source.h"
#include "obex-server-source-private.h"


/* GSourceFuncs */

static gboolean btctl_obexserver_source_prepare (GSource *source, gint *timeout);
static gboolean btctl_obexserver_source_check (GSource *source);
static gboolean btctl_obexserver_source_dispatch (GSource *source,
        BtctlObexserverSourceFunc callback, gpointer user_data);

static void get_peer_bdaddr (BtctlObexserverSource *s, bdaddr_t *addr);
static void close_client_connection (BtctlObexserverSource *s);
static void btctl_obexserver_source_finalize (GSource *source);
static void obex_event (obex_t *handle, obex_object_t *object,
        int mode, int event, int obex_cmd, int obex_rsp);
static void server_request(BtctlObexserverSource *s, obex_object_t *object,
        int event, int cmd);
static void request_hint(BtctlObexserverSource *s, obex_object_t *object,
        int event, int cmd);
static gboolean invoke_callback (BtctlObexserverSource *s, gint ev,
    gint cmd, gpointer data);

static void set_state_idle (BtctlObexserverSource *s);

/* code pinched from obex lib */
static int accept_btobex (obex_t *ob);

static GSourceFuncs gobex_source = {
    btctl_obexserver_source_prepare,
    btctl_obexserver_source_check,
    (gboolean (*)(GSource *, GSourceFunc, gpointer))
        btctl_obexserver_source_dispatch,
    btctl_obexserver_source_finalize,
    NULL,
    NULL
};

static gboolean
invoke_callback (BtctlObexserverSource *s, gint ev, gint cmd, gpointer data)
{
    BtctlObexserverEvent event;
    if (!s->callback)
        return FALSE;
    event.event = ev;
    event.cmd = cmd;
    event.data = data;
    return (*s->callback)(s, &event, s->callback_data);
}

static void
server_request (BtctlObexserverSource *s, obex_object_t *obj,
        int event, int cmd)
{
    switch (cmd) {
        case OBEX_CMD_CONNECT:
        case OBEX_CMD_DISCONNECT:
            invoke_callback (s, OBEX_EV_REQ, cmd, obj);
            OBEX_ObjectSetRsp(obj, OBEX_RSP_SUCCESS, OBEX_RSP_SUCCESS);
            break;

        case OBEX_CMD_PUT:
            /* g_message ("PUT happened"); */
            invoke_callback (s, OBEX_EV_REQ, cmd, obj);
            OBEX_ObjectSetRsp(obj, OBEX_RSP_SUCCESS, OBEX_RSP_SUCCESS);
            break;

        default:
            /* g_warning ("Rejecting request %d", cmd); */
            OBEX_ObjectSetRsp(obj, OBEX_RSP_NOT_IMPLEMENTED,
                    OBEX_RSP_NOT_IMPLEMENTED);
            break;
    }
}

static void
request_hint (BtctlObexserverSource *s, obex_object_t *obj,
        int event, int cmd)
{
    switch (cmd) {
        case OBEX_CMD_PUT:
            /* g_message ("Wants to PUT"); */
            if (invoke_callback (s, event, cmd, obj)) {
                OBEX_ObjectSetRsp (obj, OBEX_RSP_CONTINUE,
                    OBEX_RSP_SUCCESS);
            } else {
                OBEX_ObjectSetRsp (obj, OBEX_RSP_NOT_IMPLEMENTED,
                    OBEX_RSP_NOT_IMPLEMENTED);
            }

            break;
        case OBEX_CMD_CONNECT:
        case OBEX_CMD_DISCONNECT:
            OBEX_ObjectSetRsp (obj, OBEX_RSP_CONTINUE, OBEX_RSP_SUCCESS);
            invoke_callback (s, event, cmd, obj);
            break;
        default:
            OBEX_ObjectSetRsp (obj, OBEX_RSP_NOT_IMPLEMENTED,
                    OBEX_RSP_NOT_IMPLEMENTED);
            break;
    }
}

static void
obex_event (obex_t *handle, obex_object_t *obj,
        int mode, int event, int obex_cmd, int obex_rsp)
{
    BtctlObexserverSource *s =(BtctlObexserverSource*)
        OBEX_GetUserData(handle);

    switch (event) {
        case OBEX_EV_PROGRESS:
            /* g_message ("Progress"); */
            invoke_callback (s, event, 0, obj);
            break;

        case OBEX_EV_REQHINT:
            /* g_message ("Request hint, cmd %d", obex_cmd); */
            request_hint (s, obj, event, obex_cmd);
            break;

        case OBEX_EV_REQ:
            /* g_message ("Incoming request, command %d", obex_cmd); */
            server_request (s, obj, event, obex_cmd);
            break;

        case OBEX_EV_REQDONE:
            /* g_message ("Request finished"); */
            invoke_callback (s, OBEX_EV_REQDONE, obex_cmd, obj);
            break;

        case OBEX_EV_PARSEERR:
            /* g_warning ("Malformed incoming data"); */
            invoke_callback (s, event, obex_cmd, obj);
            break;

        case OBEX_EV_ABORT:
            /* g_message ("Request aborted"); */
            invoke_callback (s, event, obex_cmd, obj);
			/* workaround ABORT handling bug in openobex lib */
			set_state_idle (s);
            break;

        case OBEX_EV_STREAMEMPTY:
            /* g_message ("OBEX_EV_STREAMEMPTY"); */
            invoke_callback (s, event, 0, obj);
            break;

        case OBEX_EV_STREAMAVAIL:
            /* g_message ("OBEX_EV_STREAMAVAIL"); */
            invoke_callback (s, event, 0, obj);
            break;

        case OBEX_EV_LINKERR:
            /* g_warning ("OBEX_EV_LINKERR"); */
            invoke_callback (s, event, 0, obj);
            break;

	case OBEX_EV_REQCHECK:
	    /* g_warning ("OBEX_EV_REQCHECK"); */
	    break;

        default:
            g_warning ("Unhandled OBEX event %d", event);
            break;
    }
}


static gboolean
btctl_obexserver_source_prepare (GSource *source, gint *timeout)
{
    return FALSE;
}

static void
get_peer_bdaddr (BtctlObexserverSource *s, bdaddr_t *addr)
{
    struct sockaddr_rc sa;
    socklen_t salen;

    salen = sizeof (sa);
    if (getpeername (s->fd.fd, (struct sockaddr *)&sa, &salen) < 0)
    {
        g_error ("Couldn't get peer name, err %d", errno);
    }
    bacpy(addr, &sa.rc_bdaddr);
}

/* warning: these  two functions use internals from openobex */

static void
set_state_idle (BtctlObexserverSource *s)
{
    struct obex *obex_priv;
    obex_priv = (struct obex *)s->handle;
    obex_priv->state = MODE_SRV | STATE_IDLE;
}

static void
close_client_connection (BtctlObexserverSource *s)
{
    struct obex *obex_priv;

    /* g_message ("disconnecting client"); */
    OBEX_TransportDisconnect (s->handle);
    g_source_remove_poll ((GSource*) s, &s->fd);
    s->fd.fd = -1;
    s->peer_name[0] = 0;
    obex_priv = (struct obex *)s->handle;
    obex_priv->state = MODE_SRV | STATE_IDLE;
    if (obex_priv->object != NULL) {
        /* no idea why this sometimes isn't null */
        OBEX_ObjectDelete (s->handle, obex_priv->object);
        obex_priv->object = NULL;
    }
}

/**
 * btctl_obexserver_client_cancel:
 * @s: BtctlObexserverSource
 *
 * Requests cancellation of the running operation.
 **/
void
btctl_obexserver_source_client_cancel (BtctlObexserverSource *s)
{
    OBEX_CancelRequest (s->handle, FALSE);
}

/**
 * btctl_obexserver_client_close:
 * @s: BtctlObexserverSource
 *
 * If a client connection is active, forcibly closes it.
 **/
void
btctl_obexserver_source_client_close (BtctlObexserverSource *s)
{
    if (s->fd.fd >= 0)
        close_client_connection (s);
}

static int
accept_btobex (obex_t *ob)
{
	struct obex *handle = (struct obex *)ob;
	int addrlen = sizeof(struct sockaddr_rc);
	handle->fd = accept (handle->serverfd,
			(struct sockaddr *)&handle->trans.peer.rfcomm,
			&addrlen);
	if (handle->fd < 0) {
		return -1;
	}

	handle->trans.mtu = OBEX_DEFAULT_MTU;
	return 0;
}


/**
 * btctl_obexserver_source_check:
 * @source: GSource being polled
 *
 * 
 *
 * Return value: TRUE if we should go and dispatch.
 **/
static gboolean
btctl_obexserver_source_check (GSource *source)
{
    BtctlObexserverSource *s = (BtctlObexserverSource*)source;
    int ret;

    if (! s->initialised)
        return FALSE;

    /* some boring debug checks */
	/*
	if (s->serverfd.revents & G_IO_ERR)
        g_message ("G_IO_ERR on server");
    if (s->serverfd.revents & G_IO_IN)
        g_message ("G_IO_IN on server");
    if (s->serverfd.revents & G_IO_HUP)
        g_message ("G_IO_HUP on server");

    if (s->fd.fd >= 0 && (s->fd.revents & G_IO_ERR))
        g_message ("G_IO_ERR on client");
    if (s->fd.fd >= 0 && (s->fd.revents & G_IO_IN))
        g_message ("G_IO_IN on client");
    if (s->fd.fd >= 0 && (s->fd.revents & G_IO_HUP))
        g_message ("G_IO_HUP on client");
		*/

    if (s->serverfd.revents & G_IO_IN) {
        /* an event happened on the server file descriptor */
        /* g_message ("accepting client connection"); */

        /* accept the connection and create a new client socket.
		 * this is the only use of OBEX code here Bluetooth-
		 * specific.  It basically needs to be a copy of OBEX's
		 * transport_accept if we ever use this code to be a
		 * general obex handler */
        ret = accept_btobex (s->handle);

        if (ret < 0) {
            g_warning ("Error while accepting client connection");
        } else {
            /* now ensure we poll on this one too: GetFD magically knows
             * to return the client FD when one is available */
            s->fd.fd = OBEX_GetFD (s->handle);
            s->fd.events = G_IO_IN | G_IO_HUP | G_IO_ERR;
            s->fd.revents = 0;

            /* store away friendly details of our peer */
            get_peer_bdaddr (s, &s->peer_bdaddr);
            ba2str (&s->peer_bdaddr, s->peer_name);

            /* g_message ("accepted peer is %s", s->peer_name); */
            g_source_add_poll ((GSource*) s, &s->fd);
        }

        /* we may choose, at this point, to notify via callback
         * that an acceptance of a socket has been made */
 
    } else if (s->fd.fd >= 0) {
        if (s->fd.revents & G_IO_IN) {
            /* an event on the client descriptor */

            /* read the data: this dispatches a bunch of stuff
             * to the obex_event handler. */

            return TRUE;

            /* ret = obex_data_indication (s->handle, NULL, 0); */

        } else if (s->fd.revents & G_IO_HUP) {
            /* end of transmission */
            close_client_connection (s);
        }
    }

    return FALSE;
}

static gboolean
btctl_obexserver_source_dispatch (GSource *source,
        BtctlObexserverSourceFunc callback,
        gpointer user_data)
{
    BtctlObexserverSource *s = (BtctlObexserverSource*)source;
    int ret;
	char bufc;
    /* g_message ("btctl_obexserver_source_dispatch"); */

	/* this call is a hack to make it read the data when we
	   want.  The bufc buffer is never used, as with a Bluetooth
	   transport OBEX does its own thing.  But we need it to
	   be non-null in order to use this function */
	ret = OBEX_CustomDataFeed (s->handle, &bufc, 0);

    return TRUE;
}



static void
btctl_obexserver_source_finalize (GSource *source)
{
    BtctlObexserverSource *s = (BtctlObexserverSource*)source;
    /* g_message ("btctl_obexserver_source_finalize"); */

    if (! s->initialised)
        return;

    OBEX_Cleanup (s->handle);
}

void
btctl_obexserver_source_destroy (BtctlObexserverSource *source)
{
    /* g_message ("btctl_obexserver_source_destroy"); */
    g_source_destroy ((GSource *)source);
}

void
btctl_obexserver_source_attach (BtctlObexserverSource *source,
        GMainContext *ctxt)
{
    g_source_attach ((GSource *)source, ctxt);
}

BtctlObexserverSource *
btctl_obexserver_source_new ()
{
    BtctlObexserverSource *source;
    int err;

    source = (BtctlObexserverSource*) g_source_new (
            &gobex_source, sizeof(BtctlObexserverSource));
    if (!source) {
        g_assert_not_reached();
        return NULL;
    }

    source->initialised = FALSE;

    g_source_set_can_recurse ((GSource *)source, FALSE);

    source->handle = OBEX_Init (OBEX_TRANS_BLUETOOTH, obex_event, 0);

    if (! source->handle) {
        return source;
    }

    /* ensure handler routines can get back at the GSource if
     * they need to. */
    OBEX_SetUserData (source->handle, (void *)source);

    /* register on channel 4 */
    err = BtOBEX_ServerRegister (source->handle, BDADDR_ANY, 4);

    if (err < 0) {
	if (err == -ESOCKTNOSUPPORT) {
          g_warning ("OBEX server register error: openobex compiled without Bluetooth support");
	} else {
          g_warning ("OBEX server register error: %d", err);
	}
        btctl_obexserver_source_unref (source);
        return NULL;
    }

    source->initialised = TRUE;

    source->fd.fd = -1;

    source->serverfd.fd = OBEX_GetFD (source->handle);
    source->serverfd.events = G_IO_IN | G_IO_HUP | G_IO_ERR;
    source->serverfd.revents = 0;

    g_source_add_poll ((GSource*) source, &source->serverfd);

    return source;
}

void
btctl_obexserver_source_set_callback (BtctlObexserverSource *source,
        BtctlObexserverSourceFunc func, gpointer data,
        GDestroyNotify notify)
{
    g_source_set_callback ((GSource*)source,
            (GSourceFunc)func, data, notify);
    source->callback = func;
    source->callback_data = data;
}

