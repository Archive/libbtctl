/*
 * libbtctl -- GObject Bluetooth libraries
 * Copyright (C) 2003-2004 Edd Dumbill <edd@usefulinc.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/stat.h>

#include <glib.h>
#include <glib-object.h>

#include "btobex-client.h"
#include "btobex.h"

/*
 * This program is a command-line only OBEX client
 * as a test frame for the BtctlObexClient object.
 */

static char *hellodata = "Hello, World!\n";
static GMainLoop *loop;

static void
progress_callback (BtctlObexClient *bo, gchar * bdaddr, gpointer data) 
{
	g_message ("Progress from %s", bdaddr);
}

static gboolean
quit_me (BtctlObexClient *bo)
{
	g_main_loop_quit (loop);
	return FALSE;
}

static void
complete_callback (BtctlObexClient *bo, gchar * bdaddr, gpointer data) 
{
	g_message ("Operation complete from %s", bdaddr);
	g_idle_add ((GSourceFunc) quit_me, (gpointer) bo);
}

static gboolean
send_hello (BtctlObexClient *bo)
{
	if (btctl_obex_client_push_data (bo, "hello.txt", hellodata,
			strlen (hellodata))) {
		g_message ("PUSH request succeeded.");
	} else {
		g_message ("PUSH request failed.");
	}
	return FALSE;
}

static void
connected_callback (BtctlObexClient *bo, gchar * bdaddr, gpointer data) 
{
	g_message ("Connected OK to %s", bdaddr);
	g_idle_add ((GSourceFunc) send_hello, (gpointer) bo);
}

static void
error_callback (BtctlObexClient *bo, gchar * bdaddr, guint reason, gpointer data) 
{
	g_message ("Error %d from %s",reason,  bdaddr);
	switch (reason) {
		case BTCTL_OBEX_ERR_LINK:
			g_message ("Link error");
			break;
		case BTCTL_OBEX_ERR_PARSE:
			g_message ("Malformed incoming data");
			break;
		case BTCTL_OBEX_ERR_ABORT:
			g_message ("Client aborted transfer");
			break;
	}
}

int
main(int argc, char **argv)
{
	BtctlObexClient *bo;

	g_type_init();

	bo=btctl_obex_client_new_and_connect ("00:40:05:C1:4E:ED", 4);

	g_signal_connect (G_OBJECT(bo), "progress",
				G_CALLBACK(progress_callback), NULL);
	g_signal_connect (G_OBJECT(bo), "complete",
				G_CALLBACK(complete_callback), NULL);
	g_signal_connect (G_OBJECT(bo), "connected",
				G_CALLBACK(connected_callback), NULL);
	g_signal_connect (G_OBJECT(bo), "error",
				G_CALLBACK(error_callback), NULL);

	if (btctl_obex_client_is_initialised (bo))
	{
		g_message ("About to send file...");
		loop = g_main_loop_new (NULL, FALSE);
		g_main_loop_run (loop);
	} else {
		g_warning ("No OBEX client initialised, quitting.");
	}

	g_object_unref(bo);
	return 0;
}
