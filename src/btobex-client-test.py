#!/usr/bin/python

import sys
import btctl
import gtk
import gobject

# example obex client in python

def progress_callback (bo, bdaddr):
    print ("got progress from %s" % (bdaddr))

def error_callback (bo, bdaddr, err):
    print ("got error %d from %s" % (err, bdaddr))

def complete_callback (bo, bdaddr):
    print ("transfer complete from %s" % (bdaddr))
    gobject.idle_add (quit_me, bo)

def connected_callback (bo, bdaddr):
    print ("connected ok to %s" % (bdaddr))
    gobject.idle_add (send_hello, bo)

# idle handlers to perform functions

def quit_me (bo):
    gtk.main_quit ()

def send_hello (bo):
    bo.push_data ("hello.txt", "Hello, Python World!")

bo = btctl.ObexClient("00:40:05:C1:4E:ED", 4)
bo.connect("progress", progress_callback);
bo.connect("error", error_callback);
bo.connect("complete", complete_callback);
bo.connect("connected", connected_callback);

if not bo.is_initialised():
  print ("Client not initialised")
  sys.exit (1)



gtk.main ()
