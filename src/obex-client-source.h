#ifndef _BTCTL_OBEXCLIENT_SOURCE_H
#define _BTCTL_OBEXCLIENT_SOURCE_H

#include <sys/socket.h>
#include <bluetooth/bluetooth.h>
#include <bluetooth/hci.h>

#include <glib.h>
#include <openobex/obex.h>

G_BEGIN_DECLS

typedef struct _BtctlObexclientEvent BtctlObexclientEvent;

struct _BtctlObexclientEvent {
    gint event;
    gint cmd;
    gpointer data;
};

typedef struct _BtctlObexclientSource BtctlObexclientSource;

typedef gboolean (*BtctlObexclientSourceFunc) 
    (struct _BtctlObexclientSource *source, BtctlObexclientEvent *event,
     gpointer data);

struct _BtctlObexclientSource {
    GSource gsource;
    GPollFD fd;
    gboolean initialised;
    obex_t *handle;
    bdaddr_t peer_bdaddr;
    gchar peer_name[20];
    BtctlObexclientSourceFunc callback;
    gpointer callback_data;
    guint to_send;
    guint sent;
    const guchar * send_data;
};


#define btctl_obexclient_source_ref(x) g_source_ref((GSource*)x)
#define btctl_obexclient_source_unref(x) g_source_unref((GSource*)x)

void btctl_obexclient_source_destroy (BtctlObexclientSource *source);
void btctl_obexclient_source_attach (BtctlObexclientSource *source,
        GMainContext *ctxt);
BtctlObexclientSource * btctl_obexclient_source_new (gchar *bdaddr, gint channel);
void btctl_obexclient_source_set_callback (BtctlObexclientSource *source,
        BtctlObexclientSourceFunc func, gpointer data,
        GDestroyNotify notify);
void btctl_obexclient_source_connect (BtctlObexclientSource *bc);
void btctl_obexclient_source_disconnect (BtctlObexclientSource *bc);
void btctl_obexclient_source_disconnect_forcibly (BtctlObexclientSource *bc);
gboolean btctl_obexclient_source_push (BtctlObexclientSource *bc, gchar *fname, const guchar * data, guint len);

G_END_DECLS

#endif /* _BTCTL_OBEXCLIENT_SOURCE_H */
