/*
 * libbtctl -- GObject Bluetooth libraries
 * Copyright (C) 2003-2004 Edd Dumbill <edd@usefulinc.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <fcntl.h>
#include <errno.h>
#include <ctype.h>

#include <termios.h>
#include <fcntl.h>
#include <getopt.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <asm/types.h>
#include <netinet/in.h>


#include <bluetooth/bluetooth.h>
#include <bluetooth/hci.h>
#include <bluetooth/hci_lib.h>
#include <bluetooth/rfcomm.h>
#undef RFCOMM_PSM
#include <bluetooth/sdp.h>
#include <bluetooth/sdp_lib.h>

#include <glib.h>


#include "btctl.h"
#include "btctlimpl.h"
#include "btctl-discovery-source.h"


struct search_context {
	char        *svc;       /* Service */
	uuid_t      group;      /* Browse group */
	int         tree;       /* Display full attribute tree */
	uint32_t    handle;     /* Service record handle */
	int found;
};

struct report_context {
	BtctlController *bc;
	char *id;
	guint chan;
};

typedef int (*handler_t) (BtctlController *bc, bdaddr_t *bdaddr, struct search_context *arg, GError **err);

static int do_browse(BtctlController *bc, bdaddr_t *bdaddr, void *arg, GError **err);

static int do_search(BtctlController *bc, bdaddr_t *bdaddr, struct search_context *context, GError **err);

void fire_got_device(char *devaddr);
void fire_got_device_name(char *devaddr, char *name);

/* main async discovery callback handler */
static gboolean cb_hci_data (BtctlDiscoverySource *source, gpointer user_data);
/* aysnc hci subhandlers */
static void name_request (BtctlController *bc, bdaddr_t *bdaddr);
static void inquiry_result (BtctlController *bc, int dd, unsigned char *buf,
        int len);
static void inquiry_result_with_rssi (BtctlController *bc, int dd,
	unsigned char *buf, int len);
static void conn_request (BtctlController *bc, int dd, unsigned char *buf,
        int len);
static void conn_complete (BtctlController *bc, int dd, unsigned char *buf,
        int len);
static void name_complete (BtctlController *bc, int dd, unsigned char *buf,
        int len);
static void inquiry_complete (BtctlController *bc, int dd);


void fire_got_device(char *devaddr) {
    printf("DEVICE: %s\n", devaddr);
}

void fire_got_device_name(char *devaddr, char *name) {
    printf("NAME  : %s - %s\n", devaddr, name);
}


static void 
print_service_class(void *value, void *userData)
{
	char ServiceClassUUID_str[MAX_LEN_SERVICECLASS_UUID_STR];
	char UUID_str[MAX_LEN_UUID_STR];

	uuid_t *uuid = (uuid_t *)value;

	sdp_uuid2strn(uuid, UUID_str, MAX_LEN_UUID_STR);
	sdp_svclass_uuid2strn(uuid, ServiceClassUUID_str, MAX_LEN_SERVICECLASS_UUID_STR);
	printf("  \"%s\" (0x%s)\n", ServiceClassUUID_str, UUID_str);
}

static void 
btctl_controller_impl_report_class(void *value, void *userData)
{
	struct report_context *con=(struct report_context *)userData;
	BtctlController *bc=con->bc;

	char ServiceClassUUID_str[MAX_LEN_SERVICECLASS_UUID_STR];
	char UUID_str[MAX_LEN_UUID_STR];

	uuid_t *uuid = (uuid_t *)value;

	sdp_uuid2strn(uuid, UUID_str, MAX_LEN_UUID_STR);
	sdp_svclass_uuid2strn(uuid, ServiceClassUUID_str, MAX_LEN_SERVICECLASS_UUID_STR);

	btctl_controller_got_device_service(bc, con->id, ServiceClassUUID_str,
					    (guint)(uuid->value.uuid16),
					    con->chan);
}

static void 
print_service_desc(void *value, void *user)
{
	char str[MAX_LEN_PROTOCOL_UUID_STR];
	char UUID_str[MAX_LEN_UUID_STR];
	guint *chan=(guint*)user;

	sdp_data_t *p = (sdp_data_t *)value, *s;
	int i = 0, proto = 0;
	
	for (; p; p = p->next, i++) {
		switch (p->dtd) {
		case SDP_UUID16:
		case SDP_UUID32:
		case SDP_UUID128:
			sdp_uuid2strn(&p->val.uuid, UUID_str, MAX_LEN_UUID_STR);
			sdp_proto_uuid2strn(&p->val.uuid, str, sizeof(str));
			proto = sdp_uuid_to_proto(&p->val.uuid);
			printf("  \"%s\" (0x%s)\n", str, UUID_str);
			break;
		case SDP_UINT8:
		  if (proto == RFCOMM_UUID) {
			printf("    Channel: %d\n", p->val.uint8);
			*chan=(guint)p->val.uint8;
		  } else {
			printf("    uint8: 0x%x\n", p->val.uint8);
		  }
		  break;
		case SDP_UINT16:
			if (proto == L2CAP_UUID) {
				if (i == 1)
					printf("    PSM: %d\n", p->val.uint16);
				else
					printf("    Version: 0x%04x\n", p->val.uint16);
			} else if (proto == BNEP_UUID)
				if (i == 1)
					printf("    Version: 0x%04x\n", p->val.uint16);
				else
					printf("    uint16: 0x%x\n", p->val.uint16);
			else
				printf("    uint16: 0x%x\n", p->val.uint16);
			break;
		case SDP_SEQ16:
			printf("    SEQ16:");
			for (s = p->val.dataseq; s; s = s->next)
				printf(" %x", s->val.uint16);
			printf("\n");
			break;
		default:
			printf("    FIXME: dtd=0%x\n", p->dtd);
			break;
		}
	}
}

/*
static void 
print_lang_attr(void *value, void *user)
{
	sdp_lang_attr_t *lang = (sdp_lang_attr_t *)value;
	printf("  code_ISO639: 0x%02x\n", lang->code_ISO639);
	printf("  encoding:    0x%02x\n", lang->encoding);
	printf("  base_offset: 0x%02x\n", lang->base_offset);
}
*/

static void 
print_access_protos(void *value, void *userData)
{
	sdp_list_t *protDescSeq = (sdp_list_t *)value;
	sdp_list_foreach(protDescSeq, print_service_desc, userData);
}

/*
static void 
print_profile_desc(void *value, void *userData)
{
	sdp_profile_desc_t *desc = (sdp_profile_desc_t *)value;
	char str[MAX_LEN_PROFILEDESCRIPTOR_UUID_STR];
	char UUID_str[MAX_LEN_UUID_STR];

	sdp_uuid2strn(&desc->uuid, UUID_str, MAX_LEN_UUID_STR);
	sdp_profile_uuid2strn(&desc->uuid, str, MAX_LEN_PROFILEDESCRIPTOR_UUID_STR);

	printf("  \"%s\" (0x%s)\n", str, UUID_str);
	if (desc->version)
		printf("    Version: 0x%04x\n", desc->version);
}*/

/*
 * Parse a SDP record in user friendly form.
 */
static void 
print_service_attr(BtctlController *bc, char *id, sdp_record_t *rec)
{
	sdp_list_t *list = 0, *proto = 0;
	struct report_context con;

	sdp_record_print(rec);
	memset((void*)&con, 0, sizeof(struct report_context));
	con.id=id;
	con.bc=bc;

	printf("Service RecHandle: 0x%x\n", rec->handle);
	if (sdp_get_access_protos(rec, &proto) == 0) {
		printf("Protocol Descriptor List:\n");
		sdp_list_foreach(proto, print_access_protos, &con.chan);
		sdp_list_free(proto, (sdp_free_func_t)sdp_data_free);
	}
	if (sdp_get_service_classes(rec, &list) == 0) {
		printf("Service Class ID List:\n");
		sdp_list_foreach(list, print_service_class, 0);
		sdp_list_foreach(list, btctl_controller_impl_report_class, &con);
		sdp_list_free(list, free);
	}
	/*
	if (sdp_get_lang_attr(rec, &list) == 0) {
		printf("Language Base Attr List:\n");
		sdp_list_foreach(list, print_lang_attr, 0);
		sdp_list_free(list, free);
	}
	if (sdp_get_profile_descs(rec, &list) == 0) {
	printf("Profile Descriptor List:\n");
	sdp_list_foreach(list, print_profile_desc, 0);
	sdp_list_foreach(list, btctl_controller_impl_report_profile, &con);
	sdp_list_free(list, free);
	}
*/
}

// TODO:
//   the SDP library probably isn't threadsafe, so we should
//   implement a static mutex for when this method is called.

/*

		btctl_controller_got_device_service(bc, str,
								 btctl_controller_get_service_name(uuid->value.uuid16),
								 (guint)uuid->value.uuid16, theport);
*/

void
btctl_controller_impl_cmd_scan(BtctlController *bc, GError **err)
{
	inquiry_info *info=NULL;
	int num_rsp, length, flags;
	bdaddr_t bdaddr;
	char name[248];
	int i, dd;

	length  = 8;  /* ~10 seconds */
	num_rsp = 10;
	flags = 0;

	printf("dev_id is %d\n", bc->hci_id);

	if (bc->cancel) {
		btctl_controller_report_status(bc, BTCTL_STATUS_ERROR);
		return;
	}

	btctl_controller_report_status(bc, BTCTL_STATUS_SCANNING);

	num_rsp = hci_inquiry(bc->hci_id, length, num_rsp, NULL, &info, flags);

	if (num_rsp < 0) {
		perror("Inquiry failed.");
		btctl_controller_report_status(bc, BTCTL_STATUS_ERROR);
		return;
	}

	printf("Got %d responses.\n", num_rsp);

	if (bc->cancel) {
		btctl_controller_report_status(bc, BTCTL_STATUS_ERROR);
		free(info);
		return;
	}


	for (i = 0; i < num_rsp; i++) {
		baswap(&bdaddr, &(info+i)->bdaddr);
		printf("\t%s\tclock offset: 0x%4.4x\tclass: 0x%2.2x%2.2x%2.2x\n",
				batostr(&bdaddr), (info+i)->clock_offset,
				(info+i)->dev_class[2],
				(info+i)->dev_class[1],
				(info+i)->dev_class[0]);
		btctl_controller_got_device(bc, batostr(&bdaddr),
				(info+i)->dev_class[0]|
				((info+i)->dev_class[1]<<8)|
				((info+i)->dev_class[2]<<16));
	}

	btctl_controller_report_status(bc, BTCTL_STATUS_GETTING_NAMES);
	dd = hci_open_dev(bc->hci_id);
	if (dd>=0) {
		for (i = 0; i < num_rsp; i++) {
			memset(name, 0, sizeof(name));
			if (hci_read_remote_name(dd, &(info+i)->bdaddr,
						 sizeof(name), name, 100000) < 0)
				strcpy(name, "n/a");

			baswap(&bdaddr, &(info+i)->bdaddr);

			btctl_controller_got_device_name(bc, batostr(&bdaddr),
							 name);
			if (bc->cancel) {
				btctl_controller_report_status(bc, BTCTL_STATUS_ERROR);
				free(info);
				return;
			}
		}

		btctl_controller_report_status(bc, BTCTL_STATUS_GETTING_SERVICES);
		close(dd);
		for(i=0; i<num_rsp; i++) {
			// baswap(&bdaddr, &(info+i)->bdaddr);

			if (bc->cancel) {
				btctl_controller_report_status(bc, BTCTL_STATUS_ERROR);
				free(info);
				return;
			}
			do_browse(bc, &((info+i)->bdaddr), NULL, err);
		}
	} else {
		perror("HCI device open failed.");
		/* TODO: report this status somehow */
		btctl_controller_report_status(bc, BTCTL_STATUS_ERROR);
		free(info);
		return;
	}
	btctl_controller_report_status(bc, BTCTL_STATUS_COMPLETE);
	free(info);
}

/*
 * Perform an inquiry and search/browse all peer found.
 */
static void 
inquiry(BtctlController *bc, handler_t handler, void *arg, GError **err)
{
	inquiry_info ii[20];
	uint8_t count = 0;
	int i;

	printf("Inquiring ...\n");
	if (sdp_general_inquiry(ii, 20, 8, &count) < 0) {
		printf("Inquiry failed\n");
		return;
	}
	for (i=0; i<count; i++) {
		handler(bc, &ii[i].bdaddr, arg, err);
	}
}


/*
 * Search for a specific SDP service
 */
static int 
do_search(BtctlController *bc, bdaddr_t *bdaddr, struct search_context *context, GError **err)
{
	sdp_list_t *attrid, *search, *seq, *next;
	uint32_t range = 0x0000ffff;
	char str[20];
	sdp_session_t *sess = NULL;
	int retries=2;

	if (!bdaddr) {
		inquiry(bc, do_search, context, err);
		return 0;
	}
	while (!sess && retries > 0) {
        /* some devices, eg, Ericsson r520, need a little 'rest' if
         * they've just been used.  So we don't give up right away
         * with an SDP connect.
         */
		sess = sdp_connect(BDADDR_ANY, bdaddr, 0);
		if (!sess)
			sleep (2);
		retries--;
	}

	ba2str(bdaddr, str);
	if (!sess) {
		if (err && *err) {
			g_set_error (err, BTCTL_ERROR,
			     BTCTL_ERROR_CONNECTION_FAILED,
			     _("Bluetooth connection to remote device failed."));
		}
		return -1;
	}
	if (context->svc)
		printf("Searching for %s on %s ...\n", context->svc, str);
	else
		printf("Browsing %s ...\n", str);

	attrid = sdp_list_append(0, &range);
	search = sdp_list_append(0, &context->group);
	if (sdp_service_search_attr_req(sess, search, SDP_ATTR_REQ_RANGE, attrid, &seq)) {
		if (*err) {
			g_set_error (err, BTCTL_ERROR,
			     BTCTL_ERROR_SERVICE_SEARCH_FAILED,
			     g_strdup_printf(_("Service search failed on device %s"), str));
		}
		sdp_close(sess);
		return -1;
	}

	sdp_list_free(attrid, 0);
	sdp_list_free(search, 0);

	for (; seq; seq = next) {
		sdp_record_t *rec = (sdp_record_t *) seq->data;
		struct search_context sub_context;

		if (context->tree) {
			/* Display full tree */
			/* sdp_printf_service_attr(rec); */
		} else {
			/* Display user friendly form */
			/* btlctl_impl_report_service_attr(rec);*/

			print_service_attr(bc, str, rec);
			(context->found)++;
		}
		printf("\n");
		
		if (sdp_get_group_id(rec, &sub_context.group) != -1) {
			/* Set the subcontext for browsing the sub tree */
			memcpy(&sub_context, context, sizeof(struct search_context));
			/* Browse the next level down if not done */
			if (sub_context.group.value.uuid16 != context->group.value.uuid16)
				do_search(bc, bdaddr, &sub_context, err);
		}
		next = seq->next;
		free(seq);
		sdp_record_free(rec);
	}
	sdp_close(sess);
	return 0;
}

static guint brute_force_ids[] = {
    SERIAL_PORT_SVCLASS_ID,
    LAN_ACCESS_SVCLASS_ID,
    DIALUP_NET_SVCLASS_ID,
    IRMC_SYNC_SVCLASS_ID,
    OBEX_OBJPUSH_SVCLASS_ID,
    OBEX_FILETRANS_SVCLASS_ID,
    IRMC_SYNC_CMD_SVCLASS_ID,
    HEADSET_SVCLASS_ID,
    CORDLESS_TELEPHONY_SVCLASS_ID,
    INTERCOM_SVCLASS_ID,
    FAX_SVCLASS_ID,
    HEADSET_AGW_SVCLASS_ID,
    PANU_SVCLASS_ID,
    NAP_SVCLASS_ID,
    GN_SVCLASS_ID,
    IMAGING_SVCLASS_ID,
    IMAGING_RESPONDER_SVCLASS_ID,
    HID_SVCLASS_ID,
    CIP_SVCLASS_ID,
    PNP_INFO_SVCLASS_ID,
    0
};


static gboolean
do_browse(BtctlController *bc, bdaddr_t *bdaddr, void *arg, GError **err)
{
	struct search_context context;
	int i;
	int ret;

	memset(&context, '\0', sizeof(struct search_context));
	/* We want to browse the top-level/root  */
	sdp_uuid16_create(&(context.group), PUBLIC_BROWSE_GROUP);
	if (bdaddr)  {
		ret = do_search(bc, bdaddr, &context, err);
	} else {
		ret = do_search(bc, 0, &context, err);
	}

	if (ret < 0)
		return FALSE;

	if (context.found == 0) {
		/* Nothing found, so we'll brute-force search instead,
		 * as some devices don't have a public browse group
		 */
		for (i=0; brute_force_ids[i]; i++) {
			sdp_uuid16_create(&(context.group), brute_force_ids[i]);
			if (bdaddr)  {
				if (do_search(bc, bdaddr, &context, err) < 0)
					return FALSE;
			} else {
				if (do_search(bc, 0, &context, err) < 0)
					return FALSE;
			}
		}
	}
	return TRUE;
}

static char *rfcomm_state[] = {
	"unknown",
	"connected",
	"clean",
	"bound",
	"listening",
	"connecting",
	"connecting",
	"config",
	"disconnecting",
	"closed"
};

static char *rfcomm_flagstostr(uint32_t flags)
{
	char str[100];
	str[0] = 0;

	strcat(str, "[");

	if (flags & (1 << RFCOMM_REUSE_DLC))
		strcat(str, "reuse-dlc ");

	if (flags & (1 << RFCOMM_RELEASE_ONHUP))
		strcat(str, "release-on-hup ");

	if (flags & (1 << RFCOMM_TTY_ATTACHED))
		strcat(str, "tty-attached");
	
	strcat(str, "]");
	return g_strdup(str);
}

int
btctl_controller_impl_establish_rfcomm_connection(BtctlController *bc, const gchar *bdstr, guint channel)
{
	bdaddr_t bdaddr;
	int dev=-1;
	struct rfcomm_dev_req *req;
	struct rfcomm_dev_list_req *dl;
	struct sockaddr_rc laddr, raddr;
	int sk;
	guint alen;
	int ctl;

	ctl=socket(AF_BLUETOOTH, SOCK_RAW, BTPROTO_RFCOMM);
	if (!ctl) {
		g_warning(_("Can't open RFCOMM control socket"));
		return -1;
	}

	dl=g_malloc(sizeof(*dl) + RFCOMM_MAX_DEV * sizeof(struct rfcomm_dev_info));
	if (!dl) {
		g_warning(_("Can't allocate memory"));
		close(ctl);
		return -1;
	}
	dl->dev_num=RFCOMM_MAX_DEV;

	if (ioctl(ctl, RFCOMMGETDEVLIST, (void *) dl) < 0) {
		g_warning(_("Can't get device list"));
		g_free(dl);
		close(ctl);
		return -1;
	}
	close(ctl);

	req=g_malloc0(sizeof(*req));

	str2ba(bdstr, &bdaddr);

	bacpy(&req->dst, &bdaddr);

	laddr.rc_family = AF_BLUETOOTH;
	bacpy(&laddr.rc_bdaddr, BDADDR_ANY);
	laddr.rc_channel = 0;
	raddr.rc_family = AF_BLUETOOTH;
	bacpy(&raddr.rc_bdaddr, &bdaddr);
	raddr.rc_channel=channel;

	if ((sk = socket(AF_BLUETOOTH, SOCK_STREAM, BTPROTO_RFCOMM)) < 0) {
		g_warning("Can't create RFCOMM socket");
		return -1;
	}

	if (bind(sk, (struct sockaddr *)&laddr, sizeof(laddr)) < 0) {
		g_warning("Can't bind RFCOMM socket");
		close(sk);
		return -1;
	}

	if (connect(sk, (struct sockaddr *)&raddr, sizeof(raddr)) < 0) {
		g_warning("Can't connect RFCOMM socket");
		close(sk);
		return -1;
	}

	alen = sizeof(laddr);
	if (getsockname(sk, (struct sockaddr *)&laddr, &alen) < 0) {
		g_warning("Can't get RFCOMM socket name");
		close(sk);
		return -1;
	}

	bacpy(&req->src, &laddr.rc_bdaddr);
	bacpy(&req->dst, &raddr.rc_bdaddr);
	req->dev_id=dl->dev_num;
	req->flags=(1 << RFCOMM_REUSE_DLC) | (1 << RFCOMM_RELEASE_ONHUP);;
	req->channel=raddr.rc_channel;

	if ((dev = ioctl(sk, RFCOMMCREATEDEV, req)) < 0) {
		g_warning("Can't create RFCOMM TTY");
		close(sk);
		return -1;
	}

	close(sk);
	g_free(dl);
	g_free(req);


	return dev;
}

/*
  returns the port number of an established rfcomm connection to
  a device.
  returns -1 if nothing available, -2 if port available but connected.
*/

int
btctl_controller_impl_get_established_rfcomm_connection(BtctlController *bc, const gchar *bdstr,
											 guint channel) 
{
	bdaddr_t bdaddr;
	struct rfcomm_dev_list_req *dl;
	struct rfcomm_dev_info *di;
	int i;
	int dev=-1;
	int ctl=socket(AF_BLUETOOTH, SOCK_RAW, BTPROTO_RFCOMM);

	str2ba(bdstr, &bdaddr);

	if (!ctl) {
		g_error(_("Can't open RFCOMM control socket"));
		return -1;
	}

	dl=g_malloc(sizeof(*dl) + RFCOMM_MAX_DEV * sizeof(*di));
	if (!dl) {
		g_warning(_("Can't allocate memory"));
		close(ctl);
		return -1;
	}
	dl->dev_num=RFCOMM_MAX_DEV;
	di=dl->dev_info;

	if (ioctl(ctl, RFCOMMGETDEVLIST, (void *) dl) < 0) {
		g_warning(_("Can't get device list"));
		g_free(dl);
		close(ctl);
		return -1;
	}

	for(i=0; i<dl->dev_num && dev<0; i++, di++) {
		/* TODO: btctl needs to know which interface it's meant to use:
		   it can't just use anyone it likes. Until then we'll make the
		   (wrong) assumption we're on a one-device setup and we'll
		   always get the device we need */
		/* if (bacmp(&di->src, BDADDR_ANY) == 0) { */
		if (bacmp(&di->dst, &bdaddr)==0 && 
				(channel==0 || channel==di->channel)) {
			if ((di->flags&(1<<RFCOMM_TTY_ATTACHED))==0) {
				dev=i;
			} else {
				/* if a candidate connection exists, but it's in use
				   then time to return -2 */
				dev=-2;
			}
		}
		/* } */
	}
	g_free(dl);
	close(ctl);

	return dev;
}

void
btctl_controller_impl_list_rfcomm_connections(BtctlController *bc) 
{
	int ctl=socket(AF_BLUETOOTH, SOCK_RAW, BTPROTO_RFCOMM);
	struct rfcomm_dev_list_req *dl;
	struct rfcomm_dev_info *di;
	gchar src[18], dst[18], addr[40];

	int i;

	if (!ctl) {
		g_error(_("Can't open RFCOMM control socket"));
		return;
	}

	dl=g_malloc(sizeof(*dl) + RFCOMM_MAX_DEV * sizeof(*di));
	if (!dl) {
		g_error(_("Can't allocate memory"));
		close(ctl);
		return;
	}
	dl->dev_num=RFCOMM_MAX_DEV;
	di=dl->dev_info;

	if (ioctl(ctl, RFCOMMGETDEVLIST, (void *) dl) < 0) {
		g_error("Can't get device list");
		g_free(dl);
		close(ctl);
		return;
	}

	for(i=0; i<dl->dev_num; i++, di++) {
		gchar *fs=NULL;
		ba2str(&di->src, src); ba2str(&di->dst, dst);

		if (bacmp(&di->src, BDADDR_ANY) == 0)
			sprintf(addr, "%s", dst);
		else
			sprintf(addr, "%s -> %s", src, dst);

		if (di->flags) 
			fs=rfcomm_flagstostr(di->flags);

		g_message("rfcomm%d: %s channel %d %s %s\n",
				di->id, addr, di->channel,
				rfcomm_state[di->state], 
				di->flags ? fs : "");

		if (fs)
			g_free(fs);
	}

	g_free(dl);
	close(ctl);
}

/* lookup what channel a particular service is on */
gint
btctl_controller_impl_scan_for_service(BtctlController *bc,
        const gchar *bdstr, guint clsid, GError **err)
{
	bdaddr_t bdaddr;
	struct search_context context;

	str2ba(bdstr, &bdaddr);
	memset(&context, '\0', sizeof(struct search_context));
	sdp_uuid16_create(&(context.group), clsid);

	return (gint) do_search(bc, &bdaddr, &context, err);
}

/* Find the signal strength for a particular device */
int
btctl_controller_impl_get_signal_strength (BtctlController *bc,
		const char *str_bdaddr, GError **err)
{
	bdaddr_t bdaddr;
	guint16 handle;
	struct hci_request rq;
	struct hci_conn_info_req *cr;
	read_rssi_rp rp;
	int dd;

	str2ba(str_bdaddr, &bdaddr);

	dd = hci_open_dev(bc->hci_id);
	if (dd < 0)
	{
		g_set_error (err, 0, 0,
				_("Couldn't access the bluetooth adapter"));
		return 0;
	}

	if (hci_create_connection(dd, &bdaddr,
				0x0008 | 0x0010, 0, 0, &handle, 25000) < 0)
	{
		perror("hci_create_connection:");
		g_set_error (err, 0, 0,
				_("Couldn't create a connection to the device"));
		close (dd);
		return 0;
	}

	cr = g_malloc0(sizeof(*cr) + sizeof(struct hci_conn_info));

	bacpy(&cr->bdaddr, &bdaddr);
	cr->type = ACL_LINK;
	if (ioctl(dd, HCIGETCONNINFO, (unsigned long) cr) < 0)
	{
		hci_disconnect (dd, handle, 0x13, 10000);
		close (dd);
		g_free (cr);
		g_set_error (err, 0, 0,
				_("Couldn't connect to the device"));
		return 0;
	}

	memset(&rq, 0, sizeof(rq));
	rq.ogf    = OGF_STATUS_PARAM;
	rq.ocf    = OCF_READ_RSSI;
	rq.cparam = &cr->conn_info->handle;
	rq.clen   = 2;
	rq.rparam = &rp;
	rq.rlen   = READ_RSSI_RP_SIZE;

	if (hci_send_req(dd, &rq, 100) < 0)
	{
		hci_disconnect (dd, handle, 0x13, 10000);
		close (dd);
		g_free (cr);
		g_set_error (err, 0, 0,
				_("Couldn't get the status of the device"));
		return 0;
	}

	hci_disconnect (dd, handle, 0x13, 10000);
	close (dd);
	g_free (cr);

	return rp.rssi;
}

/* discovery source stuff */
static gint
bdaddr_comparator (gconstpointer a, gconstpointer b)
{
	const bdaddr_t *ba, *bb;
	ba = (bdaddr_t*)a;
	bb = (bdaddr_t*)b;
	return bacmp (ba, bb);
}

static void
inquiry_result(BtctlController *bc, int dd, unsigned char *buf, int len)
{
	inquiry_info *info;
	uint8_t num;
	char addr[18];
	int i;
	uint32_t devclass;
	bdaddr_t *nb;

	num = buf[0];

	for (i = 0; i < num; i++) {
		info = (void *) (buf + (sizeof(*info) * i) + 1);
		ba2str(&info->bdaddr, addr);
		devclass = (uint32_t)(info->dev_class[2]) << 16 |
			(uint32_t)(info->dev_class[1]) << 8 |
			(uint32_t)(info->dev_class[0]);
		g_message ("inquiry_result:\tbdaddr %s class %x", addr, devclass);
		nb = g_new0(bdaddr_t, 1);
		bacpy(nb, &info->bdaddr);
		/* sometimes inquiry generates multiple identical results,
		 * at least in a buggy 2.6.1 kernel, so make sure we only
		 * log each once */
		if (! g_slist_find_custom (bc->found, (gpointer)nb, bdaddr_comparator)) {
			bc->found = g_slist_append (bc->found, (gpointer)nb);
			btctl_controller_got_device (bc, addr, devclass);
		}
	}
}

static void
inquiry_result_with_rssi(BtctlController *bc, int dd, unsigned char *buf, int len)
{
	inquiry_info_with_rssi *info;
	uint8_t num;
	char addr[18];
	int i;
	uint32_t devclass;
	bdaddr_t *nb;

	num = buf[0];

	for (i = 0; i < num; i++) {
		info = (void *) (buf + (sizeof(*info) * i) + 1);
		ba2str(&info->bdaddr, addr);
		devclass = (uint32_t)(info->dev_class[2]) << 16 |
			(uint32_t)(info->dev_class[1]) << 8 |
			(uint32_t)(info->dev_class[0]);
		g_message ("inquiry_result_with_rssi:\tbdaddr %s class %x", addr, devclass);
		nb = g_new0(bdaddr_t, 1);
		bacpy(nb, &info->bdaddr);
		/* inquiry can generate multiple identical results, so make
		 * sure we only log each once */
		if (! g_slist_find_custom (bc->found, (gpointer)nb, bdaddr_comparator)) {
			bc->found = g_slist_append (bc->found, (gpointer)nb);
			btctl_controller_got_device (bc, addr, devclass);
		}
	}
}

static void
conn_request(BtctlController *bc, int dd, unsigned char *buf, int len)
{
	evt_conn_request *evt = (void *) buf;
	remote_name_req_cp cp;
	char addr[18];

	ba2str(&evt->bdaddr, addr);
	printf("conn_request:\tbdaddr %s\n", addr);

	memset(&cp, 0, sizeof(cp));
	bacpy(&cp.bdaddr, &evt->bdaddr);
	cp.pscan_rep_mode = 0x01;
	cp.pscan_mode     = 0x00;
	cp.clock_offset   = 0x0000;

	//hci_send_cmd(dd, OGF_LINK_CTL, OCF_REMOTE_NAME_REQ,
	//			REMOTE_NAME_REQ_CP_SIZE, &cp);
}

static void
name_request (BtctlController *bc, bdaddr_t *bdaddr)
{
	remote_name_req_cp cp;
	int dd = bc->source->fd.fd;

	memset(&cp, 0, sizeof(cp));
	bacpy(&cp.bdaddr, bdaddr);
	cp.pscan_rep_mode = 0x01;
	cp.pscan_mode     = 0x00;
	cp.clock_offset   = 0x0000;

	hci_send_cmd(dd, OGF_LINK_CTL, OCF_REMOTE_NAME_REQ,
				REMOTE_NAME_REQ_CP_SIZE, &cp);
}

static void
conn_complete(BtctlController *bc, int dd, unsigned char *buf, int len)
{
	evt_conn_complete *evt = (void *) buf;
	remote_name_req_cp cp;

	printf("conn_complete:\tstatus 0x%02x\n", evt->status);

	if (evt->status)
		return;

	memset(&cp, 0, sizeof(cp));
	bacpy(&cp.bdaddr, &evt->bdaddr);
	cp.pscan_rep_mode = 0x01;
	cp.pscan_mode     = 0x00;
	cp.clock_offset   = 0x0000;

	hci_send_cmd(dd, OGF_LINK_CTL, OCF_REMOTE_NAME_REQ,
				REMOTE_NAME_REQ_CP_SIZE, &cp);
	name_request (bc, &evt->bdaddr);
}

static void
pop_name (BtctlController *bc)
{
	if (bc->found) {
		name_request (bc, (bdaddr_t *)bc->found->data);
		g_free (bc->found->data);
		/* TODO: does this cause memory leaks? */
		bc->found = g_slist_remove (bc->found, bc->found->data);
	}
}

void
btctl_controller_impl_request_name (BtctlController *bc, const gchar *bdaddr)
{
	bdaddr_t bd;
	str2ba (bdaddr, &bd);
	name_request (bc, &bd);
}

static void
name_complete(BtctlController *bc, int dd, unsigned char *buf, int len)
{
	evt_remote_name_req_complete *evt = (void *) buf;
	char addr[18];

	ba2str(&evt->bdaddr, addr);
	if (!evt->status) {
		btctl_controller_got_device_name(bc, addr, (char *)evt->name);
	} else {
		/* some error happened: we'll report the name as the bdaddr */
		btctl_controller_got_device_name(bc, addr, addr);
	}
	if (bc->found) {
		/* if there are still names to look for, look for another */
		pop_name (bc);
	} else {
		btctl_controller_report_status(bc, BTCTL_STATUS_COMPLETE);
	}
}

static void
inquiry_complete (BtctlController *bc, int dd)
{
	g_message ("inquiry complete");
	btctl_controller_report_status(bc, BTCTL_STATUS_GETTING_NAMES);
	pop_name (bc);
}

static gboolean
cb_hci_data (BtctlDiscoverySource *source, gpointer user_data)
{
	hci_event_hdr *hdr;
	unsigned char *ptr;
	int len;
	BtctlController *bc=BTCTL_CONTROLLER(user_data);

	hdr = (void *) (source->buf + 1);
	ptr = source->buf + (1 + HCI_EVENT_HDR_SIZE);
	len = source->len - (1 + HCI_EVENT_HDR_SIZE);

	switch (hdr->evt) {
	case EVT_INQUIRY_RESULT:
		inquiry_result(bc, source->fd.fd, ptr, len);
		break;
	case EVT_INQUIRY_RESULT_WITH_RSSI:
		inquiry_result_with_rssi(bc, source->fd.fd, ptr,len);
		break;
	case EVT_INQUIRY_COMPLETE:
		inquiry_complete(bc, source->fd.fd);
		break;
	case EVT_CONN_REQUEST:
		conn_request(bc, source->fd.fd, ptr, len);
		break;
	case EVT_CONN_COMPLETE:
		conn_complete(bc, source->fd.fd, ptr, len);
		break;
	case EVT_REMOTE_NAME_REQ_COMPLETE:
		name_complete(bc, source->fd.fd, ptr, len);
		break;
	}

	return TRUE;
}

gboolean
btctl_controller_impl_get_discoverable(BtctlController *bc, GError **err)
{
	static struct hci_dev_info di;
	int ctl;

	/* Open HCI socket  */
	if ((ctl = socket(AF_BLUETOOTH, SOCK_RAW, BTPROTO_HCI)) < 0) {
		g_set_error (err, btctl_error_quark(),
			     BTCTL_ERROR_NO_BLUETOOTH_DEVICE,
			     "Can't open HCI socket: %s (%d)",
			     strerror(errno), errno);
		return FALSE;
	}
	
	di.dev_id = bc->hci_id;
	if (ioctl(ctl, HCIGETDEVINFO, (void *) &di) < 0) {
		g_set_error (err, btctl_error_quark(),
			     BTCTL_ERROR_NO_BLUETOOTH_DEVICE,
			     "Can't get device info on hci%d: %s (%d)\n",
			     bc->hci_id, strerror(errno), errno);
		close (ctl);
		return FALSE;
	}
	close (ctl);

	return hci_test_bit (HCI_ISCAN, &di.flags) ? TRUE : FALSE;
}

void
btctl_controller_impl_set_discoverable(BtctlController *bc,
				       gboolean discoverable,
				       GError **err)
{
	struct hci_dev_req dr;
	int ctl;
	
	/* Open HCI socket  */
	if ((ctl = socket(AF_BLUETOOTH, SOCK_RAW, BTPROTO_HCI)) < 0) {
		g_set_error (err, btctl_error_quark(),
			     BTCTL_ERROR_NO_BLUETOOTH_DEVICE,
			     "Can't open HCI socket: %s (%d)",
			     strerror(errno), errno);
		return;
	}
	
	dr.dev_id  = bc->hci_id;
	dr.dev_opt = SCAN_PAGE;
	if (discoverable)
		dr.dev_opt |= SCAN_INQUIRY;

	if (ioctl(ctl, HCISETSCAN, (unsigned long) &dr) < 0) {
		g_set_error (err, btctl_error_quark(),
			     BTCTL_ERROR_NO_BLUETOOTH_DEVICE,
			     "Can't set scan mode on hci%d: %s (%d)",
			     bc->hci_id, strerror(errno), errno);
		close (ctl);
		return;
	}
	close (ctl);
}

void
btctl_controller_impl_set_hci_device(BtctlController *bc,
				     const char *hci_device,
				     GError **err)
{
	if (hci_device) {
		bc->hci_id = hci_devid(hci_device);
		if (bc->hci_id < 0) {
			g_set_error (err, btctl_error_quark(),
				     BTCTL_ERROR_NO_BLUETOOTH_DEVICE,
				     "Can't get id of hci device %s",
				     hci_device);
			return;
		}
		return;
	}

	bc->hci_id = hci_devid("hci0");
	if (bc->hci_id < 0) {
		/* FIXME: NULL means BDADDR_ANY? */
		bc->hci_id = hci_get_route(NULL);
		
		if (bc->hci_id < 0) {
			g_set_error (err, btctl_error_quark(),
				     BTCTL_ERROR_NO_BLUETOOTH_DEVICE,
				     "Can't get device id of hci0");
			return;
		}
	}
}


void
btctl_controller_impl_init_source(BtctlController *bc)
{
	BtctlDiscoverySource *btdsrc = bc->source;

	btctl_discovery_source_set_callback (btdsrc, cb_hci_data, (gpointer)bc, NULL);
}

