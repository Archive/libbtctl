/*
 * libbtctl -- GObject Bluetooth libraries
 * Copyright (C) 2003-2004 Edd Dumbill <edd@usefulinc.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>

#include <glib.h>
#include <glib-object.h>

#include "btobex.h"
#include "btctl-types.h"

/*
 * This program is a command-line only OBEX server, designed 
 * as a test frame for the BtctlObex object.  It also works
 * reasonably well as an example of how to use the object in
 * your own code.
 */

static gchar * 
get_safe_unique_filename(char *origpath, const gchar *dir);

static void
progress_callback (BtctlObex *bo, gchar * bdaddr, gpointer data) 
{
	g_message ("Progress from %s", bdaddr);
}

static void
complete_callback (BtctlObex *bo, gchar * bdaddr, gpointer data) 
{
	g_message ("Operation complete from %s", bdaddr);
}

static void
error_callback (BtctlObex *bo, gchar * bdaddr, guint reason, gpointer data) 
{
	g_message ("Error %d from %s",reason,  bdaddr);
	switch (reason) {
		case BTCTL_OBEX_ERR_LINK:
			g_message ("Link error");
			break;
		case BTCTL_OBEX_ERR_PARSE:
			g_message ("Malformed incoming data");
			break;
		case BTCTL_OBEX_ERR_ABORT:
			g_message ("Client aborted transfer");
			break;
	}
}

static void
request_put_callback (BtctlObex *bo, gchar * bdaddr, gpointer data) 
{

	g_message ("Device %s is about to send an object.", bdaddr);
	/* Here we'd decide whether or not to accept a PUT
	 * from this device.  For testing purposes, we will.
	 */
	btctl_obex_set_response (bo, TRUE);
}

static void
put_callback (BtctlObex *bo, gchar * bdaddr, gchar *fname,
		BtctlObexData *data, guint timestamp, gpointer user_data)
{
	int fd;
	gchar *targetname = NULL;
	g_message ("File arrived from %s", bdaddr);
	g_message ("Filename '%s' Length %d", fname, data->body_len);
	targetname = get_safe_unique_filename (fname, "/tmp");
	g_message ("Saving to '%s'", targetname);
	fd = open (targetname, O_RDWR | O_CREAT, S_IRUSR | S_IWUSR);
	g_free (targetname);
	if (fd >= 0) {
		write (fd, data->body, data->body_len);
		close (fd);
		btctl_obex_set_response (bo, TRUE);
	} else {
		g_warning ("Couldn't save file.");
		btctl_obex_set_response (bo, FALSE);
	}
}

int
main(int argc, char **argv)
{
	BtctlObex *bo;
	GMainLoop *loop;

	g_type_init();

	bo=btctl_obex_new();
	g_signal_connect (G_OBJECT(bo), "request-put",
				G_CALLBACK(request_put_callback), NULL);
	g_signal_connect (G_OBJECT(bo), "put",
				G_CALLBACK(put_callback), NULL);
	g_signal_connect (G_OBJECT(bo), "progress",
				G_CALLBACK(progress_callback), NULL);
	g_signal_connect (G_OBJECT(bo), "complete",
				G_CALLBACK(complete_callback), NULL);
	g_signal_connect (G_OBJECT(bo), "error",
				G_CALLBACK(error_callback), NULL);

	if (btctl_obex_is_initialised (bo))
	{
		g_message ("Initialised OK.  Hit Ctrl-C to terminate.");
		loop = g_main_loop_new (NULL, FALSE);
		g_main_loop_run (loop);
	} else {
		g_warning ("No OBEX initialised, quitting.");
	}

	g_object_unref(bo);
	return 0;
}

/** Utility methods **/

static gchar *
newname(const gchar *str)
{
	gchar *rightparen;
	gchar *leftparen;
	gchar *res;
	gchar *suffix;
	gchar *work;

	work=g_strdup(str);
	res=NULL;
	suffix=g_strrstr(work, ".");
	if (suffix) {
		*suffix++=0;
	}
	rightparen=g_strrstr(work, ")");
	if (rightparen) {
		leftparen=g_strrstr(work, " (");
		if (leftparen && (rightparen-leftparen)>2) {
			unsigned int i;
			gboolean isnum=TRUE;
			gchar *c=g_strndup(&leftparen[2], 
					(rightparen-leftparen-2));
			for (i=0; i<strlen(c); i++) {
				if (!g_ascii_isdigit(c[i])) {
					isnum=FALSE;
					break;
				}
			}
			if (isnum) {
				int l=strtol(c, NULL, 10);
				if (l>=1) {
					l++;
					*leftparen=0;
					if (suffix) {
						res=g_strdup_printf("%s (%d).%s",
								work, l, suffix);
					} else {
						res=g_strdup_printf("%s (%d)",
								work, l);
					}
				}
			}
			g_free(c);
		}
	}
	if (res==NULL) {
		if (suffix) {
			res=g_strdup_printf("%s (1).%s", work, suffix);
		} else {
			res=g_strdup_printf("%s (1)", work);
		}
	}
	g_free(work);
	return res;	
}

static gchar *
get_safe_unique_filename(char *origpath, const gchar *dir)
{
	gboolean done=FALSE;
	gchar *res=NULL;
	gchar *base=g_strdup(g_basename(origpath));

	while (!done) {
		res=g_build_filename(dir, base, NULL);
		if (g_file_test((const gchar*)res, G_FILE_TEST_EXISTS)) {
			gchar *newbase=newname(base);
			g_free(res);
			g_free(base);
			base=newbase;
		} else {
			done=TRUE;
		}
	}

	g_free(base);
	return res;
}

