#ifndef _BTCTL_DISCOVERY_SOURCE_H
#define _BTCTL_DISCOVERY_SOURCE_H

#include <sys/socket.h>

#include <glib.h>

G_BEGIN_DECLS

typedef struct _BtctlDiscoverySource BtctlDiscoverySource;

typedef gboolean (*BtctlDiscoverySourceFunc) 
    (BtctlDiscoverySource *source, gpointer data);

#define btctl_discovery_source_ref(x) g_source_ref((GSource*)x)
#define btctl_discovery_source_unref(x) g_source_unref((GSource*)x)

void btctl_discovery_source_send_inquiry (BtctlDiscoverySource *source);
void btctl_discovery_source_cancel_inquiry (BtctlDiscoverySource *source);
void btctl_discovery_source_destroy (BtctlDiscoverySource *source);
void btctl_discovery_source_attach (BtctlDiscoverySource *source,
        GMainContext *ctxt);
BtctlDiscoverySource * btctl_discovery_source_new (void);
void btctl_discovery_source_set_callback (BtctlDiscoverySource *source,
        BtctlDiscoverySourceFunc func, gpointer data,
        GDestroyNotify notify);
gboolean btctl_discovery_source_is_initialised (BtctlDiscoverySource *source);

G_END_DECLS

#endif /* _BTCTL_DISCOVERY_SOURCE_H */
