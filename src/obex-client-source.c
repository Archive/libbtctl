/*
 * libbtctl -- GObject Bluetooth libraries
 * Copyright (C) 2003-2004 Edd Dumbill <edd@usefulinc.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <signal.h>
#include <sys/poll.h>
#include <sys/socket.h>

#include <bluetooth/bluetooth.h>
#include <bluetooth/hci.h>
#include <bluetooth/hci_lib.h>
#include <bluetooth/rfcomm.h>

#include <glib.h>

#include "obex-client-source.h"
#include "obex-server-source-private.h"

#define OBEX_STREAM_CHUNK       4096

/* GSourceFuncs */

static gboolean btctl_obexclient_source_prepare (GSource *source, gint *timeout);
static gboolean btctl_obexclient_source_check (GSource *source);
static gboolean btctl_obexclient_source_dispatch (GSource *source,
        BtctlObexclientSourceFunc callback, gpointer user_data);

static void close_client_connection (BtctlObexclientSource *s);
static void btctl_obexclient_source_finalize (GSource *source);
static void obex_event (obex_t *handle, obex_object_t *object,
        int mode, int event, int obex_cmd, int obex_rsp);
static void server_request(BtctlObexclientSource *s, obex_object_t *object,
        int event, int cmd);
static void request_hint(BtctlObexclientSource *s, obex_object_t *object,
        int event, int cmd);
static gboolean invoke_callback (BtctlObexclientSource *s, gint ev,
    gint cmd, gpointer data);
static void fillstream (BtctlObexclientSource *bc, obex_object_t *obj);


static GSourceFuncs gobex_source = {
    btctl_obexclient_source_prepare,
    btctl_obexclient_source_check,
    (gboolean (*)(GSource *, GSourceFunc, gpointer))
        btctl_obexclient_source_dispatch,
    btctl_obexclient_source_finalize,
    NULL,
    NULL
};

void
btctl_obexclient_source_connect (BtctlObexclientSource *bc)
{
    obex_object_t *object;
    obex_headerdata_t hd;

    if(! (object = OBEX_ObjectNew(bc->handle, OBEX_CMD_CONNECT)))   {
        g_warning ("Error creating connect request");
        return;
    }

    hd.bs = "Linux";
    if(OBEX_ObjectAddHeader(bc->handle, object, OBEX_HDR_WHO, hd, 6,
                OBEX_FL_FIT_ONE_PACKET) < 0)    {
        g_warning ("Connect: Error adding header");
        OBEX_ObjectDelete(bc->handle, object);
        return;
    }
    OBEX_Request(bc->handle, object);
}

void
btctl_obexclient_source_disconnect (BtctlObexclientSource *bc)
{
    obex_object_t *object;

    if (! (object = OBEX_ObjectNew (bc->handle, OBEX_CMD_DISCONNECT)))    {
        g_warning ("Error creating disconnect request");
        return;
    }

    OBEX_Request (bc->handle, object);
}

static gboolean
invoke_callback (BtctlObexclientSource *s, gint ev, gint cmd, gpointer data)
{
    BtctlObexclientEvent event;
    if (!s->callback)
        return FALSE;
    event.event = ev;
    event.cmd = cmd;
    event.data = data;
    return (*s->callback)(s, &event, s->callback_data);
}

static void
server_request (BtctlObexclientSource *s, obex_object_t *obj,
        int event, int cmd)
{
    switch (cmd) {
        case OBEX_CMD_CONNECT:
        case OBEX_CMD_DISCONNECT:
            invoke_callback (s, OBEX_EV_REQ, cmd, obj);
            OBEX_ObjectSetRsp(obj, OBEX_RSP_SUCCESS, OBEX_RSP_SUCCESS);
            break;

        case OBEX_CMD_PUT:
            g_message ("PUT happened");
            invoke_callback (s, OBEX_EV_REQ, cmd, obj);
            OBEX_ObjectSetRsp(obj, OBEX_RSP_SUCCESS, OBEX_RSP_SUCCESS);
            break;

        default:
            g_warning ("Rejecting request %d", cmd);
            OBEX_ObjectSetRsp(obj, OBEX_RSP_NOT_IMPLEMENTED,
                    OBEX_RSP_NOT_IMPLEMENTED);
            break;
    }
}

static void
request_hint (BtctlObexclientSource *s, obex_object_t *obj,
        int event, int cmd)
{
    switch (cmd) {
        case OBEX_CMD_PUT:
            g_message ("Wants to PUT");
            if (invoke_callback (s, event, cmd, obj)) {
                OBEX_ObjectSetRsp (obj, OBEX_RSP_CONTINUE,
                    OBEX_RSP_SUCCESS);
            } else {
                OBEX_ObjectSetRsp (obj, OBEX_RSP_NOT_IMPLEMENTED,
                    OBEX_RSP_NOT_IMPLEMENTED);
            }

            break;
        case OBEX_CMD_CONNECT:
        case OBEX_CMD_DISCONNECT:
            OBEX_ObjectSetRsp (obj, OBEX_RSP_CONTINUE, OBEX_RSP_SUCCESS);
            invoke_callback (s, event, cmd, obj);
            break;
        default:
            OBEX_ObjectSetRsp (obj, OBEX_RSP_NOT_IMPLEMENTED,
                    OBEX_RSP_NOT_IMPLEMENTED);
            break;
    }
}

static void
fillstream (BtctlObexclientSource *bc, obex_object_t *obj)
{
	obex_headerdata_t	hd;
	guint actual = 0;

	actual = OBEX_STREAM_CHUNK;
	if ((bc->sent + actual) > bc->to_send)
		actual = bc->to_send - bc->sent;

	hd.bs = &(bc->send_data[bc->sent]);

	if (actual > 0) {
		OBEX_ObjectAddHeader (bc->handle, obj, OBEX_HDR_BODY,
				hd, actual, OBEX_FL_STREAM_DATA);
		bc->sent += actual;
	} else if (actual == 0) {
		/* EOF */
		OBEX_ObjectAddHeader (bc->handle, obj, OBEX_HDR_BODY,
				hd, 0, OBEX_FL_STREAM_DATAEND);
	}
}

static void
obex_event (obex_t *handle, obex_object_t *obj,
        int mode, int event, int obex_cmd, int obex_rsp)
{
    BtctlObexclientSource *s =(BtctlObexclientSource*)
        OBEX_GetUserData(handle);

    switch (event) {
        case OBEX_EV_PROGRESS:
            /* g_message ("Progress");  */
            invoke_callback (s, event, 0, obj);
            break;

        case OBEX_EV_REQHINT:
            /* g_message ("Request hint, cmd %d", obex_cmd);  */
            request_hint (s, obj, event, obex_cmd);
            break;

        case OBEX_EV_REQ:
            /* g_message ("Incoming request, command %d", obex_cmd); */
            server_request (s, obj, event, obex_cmd);
            break;

        case OBEX_EV_REQDONE:
            /* g_message ("Request finished"); */
            invoke_callback (s, OBEX_EV_REQDONE, obex_cmd, obj);
            break;

        case OBEX_EV_PARSEERR:
            /* g_warning ("Malformed incoming data"); */
            invoke_callback (s, event, obex_cmd, obj);
            break;

        case OBEX_EV_ABORT:
            /* g_message ("Request aborted"); */
            invoke_callback (s, event, obex_cmd, obj);
            break;

        case OBEX_EV_STREAMEMPTY:
            /* g_message ("OBEX_EV_STREAMEMPTY"); */
			fillstream (s, obj);
            invoke_callback (s, event, 0, obj);
            break;

        case OBEX_EV_STREAMAVAIL:
            /* g_message ("OBEX_EV_STREAMAVAIL"); */
            invoke_callback (s, event, 0, obj);
            break;

        case OBEX_EV_LINKERR:
            /* g_warning ("OBEX_EV_LINKERR"); */
            invoke_callback (s, event, 0, obj);
            break;

        default:
            g_warning ("Unhandled OBEX event %d", event);
            break;
    }
}


static gboolean
btctl_obexclient_source_prepare (GSource *source, gint *timeout)
{
    return FALSE;
}

/* warning: this function uses internals from openobex */

static void
free_obex_resources (BtctlObexclientSource *s)
{
    struct obex *obex_priv;

    obex_priv = (struct obex *)s->handle;
    obex_priv->state = MODE_SRV | STATE_IDLE;
    if (obex_priv->object != NULL) {
        /* no idea why this sometimes isn't null */
        OBEX_ObjectDelete (s->handle, obex_priv->object);
        obex_priv->object = NULL;
    }
}

static void
close_client_connection (BtctlObexclientSource *s)
{

    g_message ("disconnecting client"); 
    OBEX_TransportDisconnect (s->handle);
    g_source_remove_poll ((GSource*) s, &s->fd);
    s->fd.fd = -1;
    s->peer_name[0] = 0;
	free_obex_resources (s);
	/* mark self unusable */
	s->initialised = FALSE;
}

/**
 * btctl_obexclient_source_disconnect_forcibly:
 * @bc: source object
 *
 * Force a close of the server connection.
 **/
void
btctl_obexclient_source_disconnect_forcibly (BtctlObexclientSource *bc)
{
	close_client_connection (bc);
}

gboolean
btctl_obexclient_source_push (BtctlObexclientSource *bc,
		gchar *fname, const guchar * data, guint len)
{
	obex_object_t	*object;
	obex_headerdata_t	hd;
	guint uname_size;
	gchar *uname;
	gchar *bfname;

	if (!bc->initialised) {
		g_warning ("Not initialised.");
		return FALSE;
	}

	bc->to_send = len;
	bc->sent = 0;
	bc->send_data = data;

	object = OBEX_ObjectNew (bc->handle, OBEX_CMD_PUT);

	if (! object) {
		g_warning ("Can't make new command.");
		return FALSE;
	}

	bfname = g_path_get_basename (fname);
	uname_size = (strlen (bfname)+1)*2;
	uname = g_malloc (uname_size);
	OBEX_CharToUnicode (uname, bfname, uname_size);

	hd.bs = uname;
	OBEX_ObjectAddHeader (bc->handle, object,
			OBEX_HDR_NAME, hd, uname_size, 0);

	hd.bq4 = len;
	OBEX_ObjectAddHeader (bc->handle, object,
			OBEX_HDR_LENGTH, hd, sizeof (uint32_t), 0);

	hd.bs = NULL;
	OBEX_ObjectAddHeader (bc->handle, object,
			OBEX_HDR_BODY, hd, 0, OBEX_FL_STREAM_START);
	
	OBEX_Request (bc->handle, object);

	g_free (uname);
	g_free (bfname);

	/* TODO: do I need to delete the object now? */
	return TRUE;
}

/**
 * btctl_obexclient_source_check:
 * @source: GSource being polled
 *
 * Return value: TRUE if we should go and dispatch.
 **/
static gboolean
btctl_obexclient_source_check (GSource *source)
{
    BtctlObexclientSource *s = (BtctlObexclientSource*)source;

    if (! s->initialised)
        return FALSE;

    /* some boring debug checks */
	/*
    if (s->fd.fd >= 0 && (s->fd.revents & G_IO_ERR))
        g_message ("G_IO_ERR on client");
    if (s->fd.fd >= 0 && (s->fd.revents & G_IO_IN))
        g_message ("G_IO_IN on client");
    if (s->fd.fd >= 0 && (s->fd.revents & G_IO_HUP))
        g_message ("G_IO_HUP on client");
	*/

    if (s->fd.fd >= 0) {
        if (s->fd.revents & G_IO_IN) {
            /* an event on the client descriptor */
            return TRUE;
        } else if (s->fd.revents & G_IO_HUP) {
            /* end of transmission */
			if (s->to_send && (s->sent < s->to_send)) {
				/* HUP before we sent everthing: we'll pretend
				 * this is an abort */
				invoke_callback (s, OBEX_EV_ABORT, 0, NULL);
			}
            close_client_connection (s);
        }
    }

    return FALSE;
}

static gboolean
btctl_obexclient_source_dispatch (GSource *source,
        BtctlObexclientSourceFunc callback,
        gpointer user_data)
{
    BtctlObexclientSource *s = (BtctlObexclientSource*)source;
    int ret;
	char bufc;
    /* g_message ("btctl_obexclient_source_dispatch"); */

	/* this call is a hack to make it read the data when we
	   want.  The bufc buffer is never used, as with a Bluetooth
	   transport OBEX does its own thing.  But we need it to
	   be non-null in order to use this function */
	ret = OBEX_CustomDataFeed (s->handle, &bufc, 0);

    return TRUE;
}

static void
btctl_obexclient_source_finalize (GSource *source)
{
    BtctlObexclientSource *s = (BtctlObexclientSource*)source;
    /* g_message ("btctl_obexclient_source_finalize"); */

	if (s->handle) {
		if (s->fd.fd >= 0) {
			OBEX_TransportDisconnect (s->handle);
			free_obex_resources (s);
		}
		OBEX_Cleanup (s->handle);
	}
}

void
btctl_obexclient_source_destroy (BtctlObexclientSource *source)
{
    /* g_message ("btctl_obexclient_source_destroy"); */
    g_source_destroy ((GSource *)source);
}

void
btctl_obexclient_source_attach (BtctlObexclientSource *source,
        GMainContext *ctxt)
{
    g_source_attach ((GSource *)source, ctxt);
}

BtctlObexclientSource *
btctl_obexclient_source_new (gchar *bdaddr, gint channel)
{
    BtctlObexclientSource *source;
    int err;
    bdaddr_t remotebdaddr;
    gchar *destaddr;

    g_return_val_if_fail (bdaddr != NULL, NULL);
    g_return_val_if_fail (channel > 0, NULL);

    destaddr = g_ascii_strup (bdaddr, strlen(bdaddr));

    source = (BtctlObexclientSource*) g_source_new (
            &gobex_source, sizeof(BtctlObexclientSource));

    if (!source) {
        g_assert_not_reached();
        return NULL;
    }

    source->initialised = FALSE;

    g_source_set_can_recurse ((GSource *)source, FALSE);

    source->handle = OBEX_Init (OBEX_TRANS_BLUETOOTH, obex_event, 0);

    if (! source->handle) {
        btctl_obexclient_source_unref (source);
        return NULL;
    }

    /* ensure handler routines can get back at the GSource if
     * they need to. */
    OBEX_SetUserData (source->handle, (void *)source);

	/* compute connection address */
	strncpy (source->peer_name, destaddr, 18);
	str2ba (destaddr, &remotebdaddr);
	/* g_message ("Connecting to %s", destaddr); */
	g_free (destaddr);

	if (bacmp(&remotebdaddr, BDADDR_ANY) == 0) {
		g_warning ("Unknown Bluetooth destination");
		return NULL;
    }

	/* connect */
	err = BtOBEX_TransportConnect (source->handle, BDADDR_ANY,
			&remotebdaddr, channel);

	if (err < 0) {
		g_warning ("Unable to make a Bluetooth connection.");
		return NULL;
	}

    source->initialised = TRUE;

	/* set up the source FD for the new connection */
    source->fd.fd = OBEX_GetFD (source->handle);
    source->fd.events = G_IO_IN | G_IO_HUP | G_IO_ERR;
    source->fd.revents = 0;

    g_source_add_poll ((GSource*) source, &source->fd);

    return source;
}

void
btctl_obexclient_source_set_callback (BtctlObexclientSource *source,
        BtctlObexclientSourceFunc func, gpointer data,
        GDestroyNotify notify)
{
    g_source_set_callback ((GSource*)source,
            (GSourceFunc)func, data, notify);
    source->callback = func;
    source->callback_data = data;
}
