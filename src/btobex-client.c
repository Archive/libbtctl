/*
 * libbtctl -- GObject Bluetooth libraries
 * Copyright (C) 2003-2004 Edd Dumbill <edd@usefulinc.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */
#include <config.h>

#include <stdio.h>
#include <glib.h>
#include <glib-object.h>
#include <assert.h>

#include "btobex-client.h"
#include "btctl-marshal.h"

static gpointer	 parent_class = NULL;

/* gobject prototypes */

static void btctl_obex_client_class_init (BtctlObexClientClass *class);
static void btctl_obex_client_init (BtctlObexClient *bc);
static void btctl_obex_client_finalize(GObject *obj);

/* other static functions */
static gboolean
maincb (BtctlObexclientSource *src, BtctlObexclientEvent *evt, gpointer data);
static gboolean
reqcb (BtctlObexclientSource *src, BtctlObexclientEvent *evt, gpointer data);
static gboolean
hintcb (BtctlObexclientSource *src, BtctlObexclientEvent *evt, gpointer data);


/* gtk signal defns */

enum {
	PROGRESS_SIGNAL,
	COMPLETE_SIGNAL,
	ERROR_SIGNAL,
	CONNECTED_SIGNAL,
	DISCONNECTED_SIGNAL,
	LAST_SIGNAL
};

static gint btctl_obex_client_signals[LAST_SIGNAL] = { 0 } ;

/* BtctlObexClient functions */
G_DEFINE_TYPE(BtctlObexClient, btctl_obex_client, G_TYPE_OBJECT)

static void
btctl_obex_client_class_init (BtctlObexClientClass *klass)
{
	GObjectClass *object_class;

	parent_class = g_type_class_ref(G_TYPE_OBJECT);

	object_class=(GObjectClass*)klass;

	btctl_obex_client_signals[PROGRESS_SIGNAL] =
		g_signal_new ("progress",
			G_OBJECT_CLASS_TYPE(klass),
			G_SIGNAL_RUN_FIRST,
			G_STRUCT_OFFSET(BtctlObexClientClass, progress),
			NULL /* accu */, NULL,
			btctl_marshal_VOID__STRING,
			G_TYPE_NONE, 1, G_TYPE_STRING);

	btctl_obex_client_signals[COMPLETE_SIGNAL] =
		g_signal_new ("complete",
			G_OBJECT_CLASS_TYPE(klass),
			G_SIGNAL_RUN_FIRST,
			G_STRUCT_OFFSET(BtctlObexClientClass, complete),
			NULL /* accu */, NULL,
			btctl_marshal_VOID__STRING,
			G_TYPE_NONE, 1, G_TYPE_STRING);

	btctl_obex_client_signals[ERROR_SIGNAL] =
		g_signal_new ("error",
			G_OBJECT_CLASS_TYPE(klass),
			G_SIGNAL_RUN_FIRST,
			G_STRUCT_OFFSET(BtctlObexClientClass, error),
			NULL /* accu */, NULL,
			btctl_marshal_VOID__STRING_UINT,
			G_TYPE_NONE, 2, G_TYPE_STRING, G_TYPE_UINT);

	btctl_obex_client_signals[CONNECTED_SIGNAL] =
		g_signal_new ("connected",
			G_OBJECT_CLASS_TYPE(klass),
			G_SIGNAL_RUN_FIRST,
			G_STRUCT_OFFSET(BtctlObexClientClass, connected),
			NULL /* accu */, NULL,
			btctl_marshal_VOID__STRING,
			G_TYPE_NONE, 1, G_TYPE_STRING);

	btctl_obex_client_signals[DISCONNECTED_SIGNAL] =
		g_signal_new ("disconnected",
			G_OBJECT_CLASS_TYPE(klass),
			G_SIGNAL_RUN_FIRST,
			G_STRUCT_OFFSET(BtctlObexClientClass, disconnected),
			NULL /* accu */, NULL,
			btctl_marshal_VOID__STRING,
			G_TYPE_NONE, 1, G_TYPE_STRING);

	object_class->finalize = btctl_obex_client_finalize;

	klass->progress = NULL;
	klass->complete = NULL;
	klass->error = NULL;
}

static void
btctl_obex_client_init (BtctlObexClient *bo)
{

}

/**
 * btctl_obex_client_new:
 *
 * Create a new Bluetooth OBEX Client object. Don't use this constructor
 * use btctl_obex_client_new_and_connect() instead.
 *
 * Return value: a pointer to the client object.
 **/
BtctlObexClient*
btctl_obex_client_new(void)
{
	return BTCTL_OBEX_CLIENT (g_object_new (btctl_obex_client_get_type(), NULL));
}

/**
 * btctl_obex_client_new_and_connect:
 * @bdaddr: Bluetooth address to connect to. A copy will be made of this string.
 * @channel: RFCOMM channel to connect to.
 *
 * Creates a new BtctlObexClient object and attempts to connect it to
 * the remote Bluetooth device.  The constructor will return as soon as
 * the Bluetooth socket is made.  To check whether this worked, use
 * btctl_obex_client_is_initialised().  After the socket is made, an OBEX
 * connection will be initiated.  On successful completion of this, the
 * #connected signal will be emitted.
 *
 * Return value: a pointer to the client object.
 **/
BtctlObexClient *
btctl_obex_client_new_and_connect (gchar *bdaddr, guint channel)
{
	BtctlObexClient *bo;

	g_return_val_if_fail (bdaddr != NULL, NULL);

	bo = btctl_obex_client_new ();
	bo->bdaddr = g_strdup (bdaddr);
	bo->channel = channel;
	bo->source = btctl_obexclient_source_new (bo->bdaddr, bo->channel);

	if (bo->source && bo->source->initialised) {
		btctl_obexclient_source_attach (bo->source, NULL);
		btctl_obexclient_source_set_callback (bo->source, maincb,
				(gpointer) bo, NULL);
		btctl_obexclient_source_connect (bo->source);
	} else {
		g_warning ("Unable to initialize OBEX client source");
	}
	return bo;
}

/**
 * btctl_obex_client_disconnect:
 * @bo: Bluetooth OBEX client object.
 *
 * Disconnect a connected Bluetooth client.  On successful completion
 * of this, the #disconnected signal will be emitted.
 *
 * Return value: boolean, true if the disconnect request was sent.
 **/
gboolean
btctl_obex_client_disconnect (BtctlObexClient *bo)
{
	g_return_val_if_fail (bo != NULL, FALSE);
	g_return_val_if_fail (bo->source && bo->source->initialised, FALSE);

	btctl_obexclient_source_disconnect (bo->source);

	return TRUE;
}

static void
btctl_obex_client_finalize(GObject *obj)
{
	BtctlObexClient *bo;

	bo=BTCTL_OBEX_CLIENT(obj);

	/* custom finalize stuff goes here
	 */
	if (bo->source) {
		btctl_obexclient_source_destroy (bo->source);
		btctl_obexclient_source_unref (bo->source);
	}

	if (bo->bdaddr)
		g_free (bo->bdaddr);

	/* call superclass' destructor */
	G_OBJECT_CLASS(parent_class)->finalize(obj);
}

static void
btctl_obex_client_emit_progress (BtctlObexClient *bo, gchar *bdaddr)
{
	g_signal_emit (G_OBJECT (bo),
		btctl_obex_client_signals[PROGRESS_SIGNAL],
		0, bdaddr);
}

static void
btctl_obex_client_emit_complete (BtctlObexClient *bo, gchar *bdaddr)
{
	g_signal_emit (G_OBJECT (bo),
		btctl_obex_client_signals[COMPLETE_SIGNAL],
		0, bdaddr);
}

static void
btctl_obex_client_emit_connected (BtctlObexClient *bo, gchar *bdaddr)
{
	g_signal_emit (G_OBJECT (bo),
		btctl_obex_client_signals[CONNECTED_SIGNAL],
		0, bdaddr);
}

static void
btctl_obex_client_emit_disconnected (BtctlObexClient *bo, gchar *bdaddr)
{
	g_signal_emit (G_OBJECT (bo),
		btctl_obex_client_signals[DISCONNECTED_SIGNAL],
		0, bdaddr);
}

static void
btctl_obex_client_emit_error (BtctlObexClient *bo, gchar *bdaddr, guint reason)
{
	g_signal_emit (G_OBJECT (bo),
		btctl_obex_client_signals[ERROR_SIGNAL],
		0, bdaddr, reason);
}

static gboolean
hintcb (BtctlObexclientSource *src, BtctlObexclientEvent *evt, gpointer data)
{
	BtctlObexClient *bo = BTCTL_OBEX_CLIENT (data);

	/* FIXME: default response is permissive */
	bo->response = TRUE;

	switch (evt->cmd) {
		case OBEX_CMD_PUT:
			break;
		default:
		   break;
	}
	
	return bo->response;
}

static gboolean
reqcb (BtctlObexclientSource *src, BtctlObexclientEvent *evt, gpointer data)
{
	BtctlObexClient *bo = BTCTL_OBEX_CLIENT (data);

	/* FIXME: default response is permissive */
	bo->response = TRUE;

	switch (evt->cmd) {
		case OBEX_CMD_PUT:
			break;
		default:
			bo->response = FALSE;
			break;
	}

	return bo->response;
}

static gboolean
maincb (BtctlObexclientSource *src, BtctlObexclientEvent *evt, gpointer data)
{
	BtctlObexClient *bo = BTCTL_OBEX_CLIENT (data);
	/* g_message ("maincb event %d command %d", evt->event, evt->cmd); */

	switch (evt->event) {
		case OBEX_EV_REQHINT:
			return hintcb (src, evt, data);
			break;
		case OBEX_EV_REQ:
			return reqcb (src, evt, data);
		case OBEX_EV_PARSEERR:
			btctl_obex_client_emit_error (bo, src->peer_name,
					BTCTL_OBEX_CLIENT_ERR_PARSE);
			break;
		case OBEX_EV_ABORT:
			btctl_obex_client_emit_error (bo, src->peer_name,
					BTCTL_OBEX_CLIENT_ERR_ABORT);
			break;
		case OBEX_EV_LINKERR:
			btctl_obex_client_emit_error (bo, src->peer_name,
					BTCTL_OBEX_CLIENT_ERR_LINK);
			break;
		case OBEX_EV_REQDONE:
			switch (evt->cmd) {
				case OBEX_CMD_PUT:
					btctl_obex_client_emit_complete (bo, src->peer_name);
					break;
				case OBEX_CMD_CONNECT:
					btctl_obex_client_emit_connected (bo, src->peer_name);
					break;
				case OBEX_CMD_DISCONNECT:
					btctl_obex_client_emit_disconnected (bo, src->peer_name);
					break;
			}
			break;
		case OBEX_EV_PROGRESS:
		case OBEX_EV_STREAMEMPTY:
			btctl_obex_client_emit_progress (bo, src->peer_name);
			break;
		default:
			break;
	}
	return TRUE;
}

/* substantial public methods */

/**
 * btctl_obex_client_is_initialised:
 * @bo: Bluetooth OBEX client object.
 *
 * Check if OBEX was able to initialise OK.
 * If not, we won't be able to do anything.
 *
 * Return value: TRUE if initialised OK.
 **/
gboolean
btctl_obex_client_is_initialised (BtctlObexClient *bo)
{
	g_return_val_if_fail (bo != NULL, FALSE);
	return bo->source && bo->source->initialised;
}


/**
 * btctl_obex_client_set_response:
 * @bo: Bluetooth OBEX client object.
 * @resp: Boolean response.
 *
 * Sets TRUE or FALSE response in signal handlers
 * that must decide whether an operation can proceed
 * or not.
 **/
void
btctl_obex_client_set_response (BtctlObexClient *bo, gboolean resp)
{
	g_assert (bo != NULL);
	bo->response = resp;
}

/**
 * btctl_obex_client_push_data:
 * @bo: BtctlObexClient object.
 * @fname: Suggested filename for transmitted data.
 * @data: Pointer to data.
 * @len: Length in bytes of data.
 *
 * Initiates an OBEX PUSH of the data.  The data is not copied: don't
 * free it until you receive the #completed signal.  If initiating the
 * command failed this is usually because another command is currently
 * in progress.
 *
 * Return value: TRUE if command submitted OK, FALSE if not.
 **/
gboolean
btctl_obex_client_push_data (BtctlObexClient *bo, gchar *fname,
		const guchar * data, guint len)
{
	return btctl_obexclient_source_push (bo->source, fname, data, len);
}


/**
 * btctl_obex_client_progress:
 * @bo: BtctlObexClient object.
 *
 * Find the progress of a transfer.
 *
 * Return value: A float from 0.0 to 1.0 depending on how complete
 * the current transfer is. 0.0 if no transfer is in operation.
 **/
gdouble
btctl_obex_client_progress (BtctlObexClient *bo)
{
	gdouble prog = 0.0;

	if (bo->source->to_send) {
		prog = (gdouble) bo->source->sent / (gdouble) bo->source->to_send;
	}

	return prog;
}

