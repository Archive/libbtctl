

#include <pygobject.h>
#include "btctl-types.h"

void btctl_register_classes (PyObject *d); 
DL_EXPORT(void) initbtctl(void);
extern PyMethodDef btctl_functions[];

static PyObject * _pybtctl_obex_data_from_value(const GValue *value);
static int _pybtctl_obex_data_to_value(GValue *value, PyObject *object);

DL_EXPORT(void)
initbtctl(void)
{
	PyObject *m, *d;

	init_pygobject ();

	m = Py_InitModule ("btctl", btctl_functions);
	d = PyModule_GetDict (m);

	pyg_register_boxed_custom(BTCTL_TYPE_OBEX_DATA,
		_pybtctl_obex_data_from_value,
		_pybtctl_obex_data_to_value);

	btctl_register_classes (d);


	if (PyErr_Occurred ()) {
		Py_FatalError ("can't initialise module btctl");
	}
}

static PyObject *
_pybtctl_obex_data_from_value(const GValue *value)
{
	BtctlObexData *obexData = (BtctlObexData *) g_value_get_boxed(value); 

	PyObject *py_ret = Py_BuildValue("s#", obexData->body, obexData->body_len);
	return py_ret;
}

static int
_pybtctl_obex_data_to_value(GValue *value, PyObject *object)
{
	BtctlObexData *obexData = g_new0(BtctlObexData, 1);

	if (!PyArg_ParseTuple(object, "s#", &obexData->body, &obexData->body_len))
	{
		return -1;
	}

	g_value_init(value, BTCTL_TYPE_OBEX_DATA);
	g_value_take_boxed(value, obexData);

	return 0;
}
