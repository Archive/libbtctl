#ifndef __BTCTL_H__
#define __BTCTL_H__

#include <glib-object.h>

#include "btctl-discovery-source.h"

G_BEGIN_DECLS

#define BTCTL_TYPE_CONTROLLER	    (btctl_controller_get_type())
#define BTCTL_CONTROLLER(obj)          (G_TYPE_CHECK_INSTANCE_CAST (obj, BTCTL_TYPE_CONTROLLER, BtctlController))
#define BTCTL_CONTROLLER_CLASS(klass)  (G_TYPE_CHECK_CLASS_CAST (klass, BTCTL_TYPE_CONTROLLER, BtctlControllerClass))
#define BTCTL_IS_CONTROLLER(obj)       (G_TYPE_CHECK_INSTANCE_TYPE (obj, BTCTL_TYPE_CONTROLLER))
#define BTCTL_IS_CONTROLLER_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE ((klass), BTCTL_TYPE_CONTROLLER))
#define BTCTL_CONTROLLER_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS ((obj), BTCTL_TYPE_CONTROLLER, BtctlControllerClass)
#define BTCTL_ERROR (btctl_error_quark ())

typedef struct _BtctlController BtctlController;
typedef struct _BtctlControllerClass BtctlControllerClass;

struct _BtctlController
{
    GObject obj;
    gint    cancel;
    int     hci_id;
    GError  *init_err;
    BtctlDiscoverySource *source;
    GSList  *found;
};

struct _BtctlControllerClass
{
  GObjectClass	parent_class;
  void (*add_device) (BtctlController *bc, const char *addr, guint clsid);
  void (*device_name) (BtctlController *bc, const char *addr, const char *name);
  void (*status_change) (BtctlController *bc, int status);
  void (*add_device_service) (BtctlController *bc, const char *addr,
		  	      const char *name, guint classid, guint port);
};

enum {
	BTCTL_ERROR_NO_BLUETOOTH_DEVICE,
	BTCTL_ERROR_CONNECTION_FAILED,
	BTCTL_ERROR_SERVICE_SEARCH_FAILED
};

/* Gtk housekeeping methods */

GType	btctl_controller_get_type	(void);
BtctlController* btctl_controller_new	(const char *hci_device);

/* fields for status */

enum {
	BTCTL_STATUS_NONE,
	BTCTL_STATUS_ERROR,
	BTCTL_STATUS_SCANNING,
	BTCTL_STATUS_GETTING_NAMES,
	BTCTL_STATUS_COMPLETE,
	BTCTL_STATUS_GETTING_SERVICES,
	BTCTL_STATUS_LAST
};

#define BTCTL_RFCOMM_NO_DEVICE -1
#define BTCTL_RFCOMM_DEVICE_IN_USE -2

/* public methods */

GQuark btctl_error_quark(void);

gboolean btctl_controller_get_discoverable(BtctlController *bc, GError **err);
void btctl_controller_set_discoverable(BtctlController *bc, gboolean discoverable, GError **err);
void btctl_controller_discover_devices(BtctlController *bc, GError **err);
void btctl_controller_list_rfcomm_connections(BtctlController *bc);
gint btctl_controller_establish_rfcomm_connection(BtctlController *bc,
				       const gchar *bdstr, guint channel);
gint btctl_controller_get_established_rfcomm_connection(BtctlController *bc,
					     const gchar *bdstr,
					     guint channel);
gboolean btctl_controller_scan_for_service(BtctlController *bc, const gchar *bdstr, guint clsid, GError **err);
void btctl_controller_cancel_discovery(BtctlController *bc);
void btctl_controller_discover_async (BtctlController *bc);
gboolean btctl_controller_is_initialised (BtctlController *bc, GError **err);
int btctl_controller_get_signal_strength (BtctlController *bc,
		const gchar *bdaddr, GError **err);
void btctl_controller_request_name (BtctlController *bc, const gchar *bdaddr);

G_END_DECLS

#endif /* __BTCTL_H__ */
