#!/bin/sh
# Run this to generate all the initial makefiles, etc.

srcdir=`dirname $0`
test -z "$srcdir" && srcdir=.

# FIXME this is gross
cat python-headers.m4 openobex.m4 >acinclude.m4

PKG_NAME="GObject Bluetooth Controller"

(test -f $srcdir/configure.in) || {
    echo -n "**Error**: Directory "\`$srcdir\'" does not look like the"
    echo " top-level directory"
    exit 1
}

REQUIRED_AUTOMAKE_VERSION=1.9 . gnome-autogen.sh
