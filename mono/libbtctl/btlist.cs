
namespace Btctl.Test {

	using System;
	using System.Runtime.InteropServices;
	using Btctl;

	public class TestApp {

		[DllImport("libgobject-2.0.so")]
		static extern void g_type_init ();

		public static void Main (string[] args)
		{
			g_type_init ();

			Controller ctl = new Controller ();

			Console.WriteLine ("Asking if initialised");
			if (ctl.IsInitialised()) {
				Console.WriteLine ("Yes, is initialised");
			}

			ctl.AddDevice += new AddDeviceHandler (Add_Device);
			ctl.StatusChange += new StatusChangeHandler (Status_Change);
			ctl.DiscoverDevices ();
		}

		static void Status_Change (object obj, StatusChangeArgs args)
		{
			Console.WriteLine ("Status {0}", args.Status);
		}

		static void Add_Device (object obj, AddDeviceArgs args)
		{
			Console.WriteLine ("Device {0} Class {1}", args.Addr, args.Clsid);
		}
		
	}
}
