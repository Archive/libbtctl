
namespace Btctl.Test {

	using System;
	using System.Runtime.InteropServices;
	using System.IO;
	using Btctl;
	using Gtk;

	public class TestApp {

		public static void Main (string[] args)
		{
			Application.Init ();

			Obex obex = new Obex ();
			obex.RequestPut += Put_Request;
			obex.Put += Put;
			obex.Error += Error;
			obex.Progress += Progress;
			obex.Complete += Complete;

			if (obex.IsInitialised) {
				Console.WriteLine ("Initialised OK.");

				Application.Run ();
			} else {
				Console.WriteLine ("Not initialised, exiting.");
			}
		}

		static void Progress (object obj, ProgressArgs args)
		{
			Console.WriteLine ("Progress from {0}", args.Bdaddr);
		}

		static void Complete (object obj, CompleteArgs args)
		{
			Console.WriteLine ("Complete from {0}", args.Bdaddr);
		}

		static void Put_Request (object obj, RequestPutArgs args)
		{
			Console.WriteLine ("Put request from {0}", args.Bdaddr);
			((Obex)obj).Response = true;
		}

		static void Put (object obj, PutArgs args)
		{
			string temporalFile = "/tmp/" + args.Fname;	
			Console.WriteLine ("Saving file {0}  from {1} with length {2}",
					temporalFile, args.Bdaddr, args.Data.BodyLen);

			FileStream stream = new FileStream(temporalFile, FileMode.OpenOrCreate);
			stream.Write(args.Data.Body, 0, args.Data.Body.Length);
			stream.Close();	
			((Obex)obj).Response = true;
		}

		static void Error (object o, ErrorArgs args)
		{
			Console.WriteLine ("Error from {0}, code {1}",
					args.Bdaddr, args.Code);
		}

	}
}
