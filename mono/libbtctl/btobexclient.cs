// sends a text file to a destination device

namespace Btctl.Test {

	using System;
	using System.Runtime.InteropServices;
	using Btctl;
	using Gtk;

	public class TestApp {

		static ObexClient obex;

		public static void Main (string[] args)
		{
			Application.Init ();
			
			obex = new ObexClient ("00:40:05:C1:4E:ED", 4);
			// obex = new ObexClient ("00:0A:D9:D4:5B:5D", 7);

			if (obex.IsInitialised) {
				Console.WriteLine ("Initialised OK.");

				obex.Error += Error;
				obex.Progress += Progress;
				obex.Complete += Complete;
				obex.Connected += Connected;

				Application.Run ();
			} else {
				Console.WriteLine ("Not initialised, exiting.");
			}
		}

		static void Progress (object obj, ProgressArgs args)
		{
			Console.WriteLine ("Progress from {0}", args.Bdaddr);
		}

		static void Complete (object obj, CompleteArgs args)
		{
			Console.WriteLine ("Complete from {0}, now quitting", args.Bdaddr);
			Application.Quit ();
		}

		static bool SendData ()
		{
			byte [] data = System.Text.Encoding.Default.GetBytes ("Hello, Mono World!\n");
			obex.PushData ("hello.txt", data);
			return false;
		}

		static void Connected (object obj, ConnectedArgs args)
		{
			Console.WriteLine ("Connected from {0}", args.Bdaddr);
			GLib.Idle.Add (new GLib.IdleHandler (SendData));
		}

		static void Error (object o, ErrorArgs args)
		{
			Console.WriteLine ("Error from {0}, code {1}",
					args.Bdaddr, args.Code);
		}

	}
}
